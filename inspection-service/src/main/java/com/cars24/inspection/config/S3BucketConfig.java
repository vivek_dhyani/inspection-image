package com.cars24.inspection.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Class S3Config.
 * 
 * @author Vivek.dhyani
 */
@Configuration
public class S3BucketConfig {

	/** The s 3 bucket config. */
	@Autowired
	private S3PropertyConfig s3BucketConfig;

	/**
	 * S 3 client.
	 *
	 * @return the amazon S 3
	 */
	@Bean
	public AmazonS3 s3client() {
		final BasicAWSCredentials awsCreds = new BasicAWSCredentials(s3BucketConfig.getAwsId(), s3BucketConfig.getAwsKey());
		final AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.fromName(s3BucketConfig.getRegion())).withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		return s3Client;
	}


	/**
	 * S3 video client
	 * @return the amazon S 3
	 */
	@Bean
	public AmazonS3 s3VideoClient() {
		final BasicAWSCredentials awsCreds = new BasicAWSCredentials(s3BucketConfig.getAwsId(), s3BucketConfig.getAwsKey());
		final AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.fromName(s3BucketConfig.getVideoBucketRegion())).withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		return s3Client;
	}
}
