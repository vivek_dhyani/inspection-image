package com.cars24.inspection.config;

import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * The Class S3BucketConfig.
 * 
 */
@Configuration
public class S3PropertyConfig {

	/** The bucket name. */
	@Value("${jsa.s3.bucket}")
	private String bucketName;

	/** The bucket name. */
	@Value("${s3.env.folder.name}")
	private String folderName;

	/** The aws id. */
	@Value("${jsa.aws.access_key_id}")
	private String awsId;

	/** The aws key. */
	@Value("${jsa.aws.secret_access_key}")
	private String awsKey;

	/** The region. */
	@Value("${jsa.s3.region}")
	private String region;

	/** The video bucket name. */
	@Value("${jsa.s3.video.bucket}")
	private String videoBucketName;

	/** The video bucket name. */
	@Value("${jsa.s3.video.region}")
	private String videoBucketRegion;

	/**
	 * Gets the bucket name.
	 *
	 * @return the bucket name
	 */
	public String getBucketName() {
		return bucketName;
	}

	/**
	 * Sets the bucket name.
	 *
	 * @param bucketName the new bucket name
	 */
	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	/**
	 * Gets the folder name.
	 *
	 * @return the folder name
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * Sets the folder name.
	 *
	 * @param folderName the new folder name
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	/**
	 * Gets the aws id.
	 *
	 * @return the aws id
	 */
	public String getAwsId() {
		return awsId;
	}

	/**
	 * Sets the aws id.
	 *
	 * @param awsId the new aws id
	 */
	public void setAwsId(String awsId) {
		this.awsId = awsId;
	}

	/**
	 * Gets the aws key.
	 *
	 * @return the aws key
	 */
	public String getAwsKey() {
		return awsKey;
	}

	/**
	 * Sets the aws key.
	 *
	 * @param awsKey the new aws key
	 */
	public void setAwsKey(String awsKey) {
		this.awsKey = awsKey;
	}

	/**
	 * Gets the region.
	 *
	 * @return the region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Sets the region.
	 *
	 * @param region the new region
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	public void setVideoBucketName(String videoBucketName) {
		this.videoBucketName = videoBucketName;
	}

	public void setVideoBucketRegion(String videoBucketRegion) {
		this.videoBucketRegion = videoBucketRegion;
	}

	public String getVideoBucketName() {
		return videoBucketName;
	}

	public String getVideoBucketRegion() {
		return videoBucketRegion;
	}
}
