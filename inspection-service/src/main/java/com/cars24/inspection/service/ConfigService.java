package com.cars24.inspection.service;

import com.cars24.inspection.entity.InspectionConfig;
import com.cars24.inspection.entity.StoreConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.constraints.Null;

@Service
public class ConfigService {

    /** The Constant logger. */
    private static final Logger log = LoggerFactory.getLogger(ConfigService.class);

    @Autowired
    private MongoTemplate mongoAdapter;

    private static final String INS_ANDROID_CLIENT = "android";
    private static final String TYPE   = "config";
    private static final String TYPE_VALIDATE   = "validation";

    public boolean isPlateMaskingSingleRequestEnable(int storeId) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("client").is(INS_ANDROID_CLIENT));
        query.addCriteria(Criteria.where("type").is(TYPE));
        InspectionConfig insConfig = mongoAdapter.findOne(query, InspectionConfig.class);
        StoreConfig plateMaskConfig = insConfig.getStoreConfig().getNumberPlateMask();
        return (plateMaskConfig.getApplyAllStores() || plateMaskConfig.getApplyAtStores().contains(storeId)) ? true : false;
    }

    public InspectionConfig getValidationConfig(@Null String client) {
        final Query query = new Query();
        if (client == null){
            client = INS_ANDROID_CLIENT;
        }
        query.addCriteria(Criteria.where("client").is(client));
        query.addCriteria(Criteria.where("type").is(TYPE_VALIDATE));
        InspectionConfig insConfig = mongoAdapter.findOne(query, InspectionConfig.class);
        return insConfig;
    }

}
