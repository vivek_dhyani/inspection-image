package com.cars24.inspection.service;

import com.cars24.inspection.config.RabbitMqConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

@Service
public class RabbitMqService {

    /** The Constant logger. */
    private static final Logger log = LoggerFactory.getLogger(S3Service.class);

    private final RabbitMqConfig rabbitMqConfig;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public RabbitMqService(final RabbitTemplate rabbitTemplate, RabbitMqConfig rabbitMqConfig) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitMqConfig = rabbitMqConfig;
    }

    public void send(Object message, String routingkey) {
        this.sendMessageToRabbit(rabbitTemplate, rabbitMqConfig.getInsExchangeName(), routingkey, message);
    }

    /**
     * @param rabbitTemplate
     * @param exchange
     * @param routingKey
     * @param data
     */
    public void sendMessageToRabbit(RabbitTemplate rabbitTemplate, String exchange, String routingKey, Object data) {
        try {
            log.info("Sending message to the queue using routingKey {}. Message= {}", routingKey, data);
            rabbitTemplate.convertAndSend(exchange, routingKey, data);
            log.info("The message has been sent to the queue.");
        } catch (Exception ex){
            ex.printStackTrace();
            log.error(ex.getMessage());
        }
    }


}
