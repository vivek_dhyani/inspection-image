package com.cars24.inspection.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.cars24.inspection.config.S3PropertyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * The Class FileStorageServiceImpl.
 * 
 * @author Vivek.Dhyani
 */
@Service
public class S3Service  {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(S3Service.class);

	/** The s 3 client. */
	@Autowired
	private AmazonS3 s3client;

	@Autowired
	private AmazonS3 s3VideoClient;

	/** The s 3 bucket config. */
	@Autowired
	private S3PropertyConfig s3BucketConfig;

	/** The mongo template. */
	@Autowired
	private MongoTemplate mongoTemplate;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.cars24.inspection.missed.service.FileStorageService#uploadFile(java.lang.
	 * String, java.lang.String)
	 */
	public Boolean uploadFile(String fileName, MultipartFile file, Boolean isVideoFile)  {
		boolean success = true;
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(file.getSize());
			AmazonS3 amazonS3Client = isVideoFile ? s3VideoClient : s3client;
			amazonS3Client.putObject(new PutObjectRequest(bucketNameWithFolder(isVideoFile), fileName, file.getInputStream(), metadata).withCannedAcl(CannedAccessControlList.Private));
		} catch (Exception ex) {
			success = false;
			logger.error("Exception while reading file errorMsg {}", ex.getMessage());
		}
		return success;
	}

	/**
	 * Bucket name with folder.
	 *
	 * @return the string
	 */
	private String bucketNameWithFolder(boolean isVideoFile) {
		final StringBuilder stringBuilder = new StringBuilder(s3BucketConfig.getBucketName());
		if (!isVideoFile){
			stringBuilder.append("/").append(s3BucketConfig.getFolderName());
		}
		return stringBuilder.toString();
	}

	public boolean fileExists(String name, Boolean isVideofile) {
		try {
			String bucketName = bucketNameWithFolder(isVideofile);
			AmazonS3 amazonS3Client = isVideofile ? s3VideoClient : s3client;
			ObjectMetadata result = amazonS3Client.getObjectMetadata(bucketName, name);
		} catch(Exception ex) {
			return false;
		}
		return true;
	}
}
