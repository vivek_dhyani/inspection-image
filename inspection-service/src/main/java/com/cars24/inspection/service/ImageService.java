package com.cars24.inspection.service;

import com.cars24.inspection.constants.ConstantFields;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.InsPlateMaskingLog;
import com.cars24.inspection.entity.OauthAccessTokens;
import com.cars24.inspection.entity.PlateMaskLog;
import com.cars24.inspection.entity.inspection.Inspection;
import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import com.cars24.inspection.entity.inspection.common.UtmSource;
import com.cars24.inspection.entity.inspection.common.additionalinfo.AdditionalInfo;
import com.cars24.inspection.util.DateUtil;
import com.cars24.inspection.util.ReflectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.cars24.inspection.util.Uniqid;

import java.sql.Timestamp;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class ImageService {

    private  static final String IMAGE_EXT_WEBP = "webp";
    private  static final String VIDEO_EXT_MP4 = "mp4";
    private  static final String IMAGETYPE_WEBP = "image/webp";
    private  static final String VIDEOTYPE_MP4 = "video/mp4";
    private  static final String UTM_SOURCE_QC = "qc";
    private  static final String UTM_SOURCE_CJEDIT = "cjedit";
    private static final String PLATE_MASKING_CATEGORY = "plateMasking";
    private static final int DS_RESPONSE_TIME_OUT = 1200;
    private static final String DS_IMAGE_MASKING_ROUTING_KEY = "vikalp";
    private static final int FLOW_TYPE_SINGLE = 1;


    private static final Map<String, String>  IMAGE_HASHKEY_TEMPLATE  = new HashMap<String, String>() {{
        put("inspectionData_exteriorTyres_mainImages_frontMain" ,"frontMain");
        put("inspectionData_exteriorTyres_mainImages_FrontLeftSide", "frontLeftSide");
        put("inspectionData_exteriortyres_mainImages_rearMain", "rearMain");
        put("inspectionData_exteriorTyres_mainImages_RearRightSide", "rearRightSide");
        put("inspectionData_exteriortyres_mainImages_rearBootOpened", "rearBootOpened");
        put("exteriorTyres_bonnetHood_additionalInfo_form_0", "bonnetHood");
        put("exteriorTyres_dickyDoorBootDoor_additionalInfo_form_0", "dickyDoorBootDoor");
        put("exteriorTyres_bumpers_front_additionalInfo_form_0", "frontBumpers");
        put("exteriorTyres_bumpers_rear_additionalInfo_form_0", "rearBumpers");
        put("exteriorTyres_lights_lhsTaillight_additionalInfo_form_0", "lhsTaillightLights");
        put("exteriorTyres_lights_rhsTaillight_additionalInfo_form_0", "rhsTaillightLights");
    }};



    /** The Constant logger. */
    private static final Logger logger = LoggerFactory.getLogger(S3Service.class);

    private final S3Service s3Adaptor;

    private final ConfigService configService;

    private final RabbitMqService amqp;

    private final MongoTemplate mongoAdapter;

    static String[] validFileExtensions = {"jpg", "jpe", "jpeg", "png", "mp4"};

    @Autowired
    public ImageService(S3Service s3Adaptor, RabbitMqService amqp, ConfigService configService, MongoTemplate mongoAdapter) {
        this.s3Adaptor = s3Adaptor;
        this.amqp = amqp;
        this.configService = configService;
        this.mongoAdapter = mongoAdapter;
    }

    public void uploadFile(ImageCreateRequestDto imgDto, Inspection insData) {
        try {
            Map<String, String>  res = new HashMap<String, String>();
            String resourcePath =  insData.getResourcePath();
            if(resourcePath == null || resourcePath.isEmpty()){
                insData.setResourcePath();
            }
            String path = "inspection/"+insData.getResourcePath() + insData.getCarCode()+"/v"+insData.getVersion()+"/"+imgDto.getSection()+"/"+imgDto.getType()+"/";
            if (imgDto.getPart() != null) {
                path +=  imgDto.getPart()+"/";
            }
            if (imgDto.getSubPart() != null) {
                path +=  imgDto.getSubPart()+"/";
            }

            if(imgDto.getImageFile() != null){
                this.validateFile(imgDto.getImageFile());
                String fileName = this.getFileName(imgDto.getImageFile(), imgDto.getTitle());
                if(fileName.isEmpty()){
                    throw new Exception("Invalid file format! for file :- "+fileName);
                }
                fileName = path+fileName;
                Boolean Updated = this.s3Adaptor.uploadFile(fileName, imgDto.getImageFile(), false);
                imgDto.setImageUrl(fileName);
            } else if(imgDto.getVideoFile() != null){
                this.validateFile(imgDto.getVideoFile());
                String fileName = this.getFileName(imgDto.getVideoFile(), imgDto.getTitle());
                if(fileName.isEmpty()){
                    throw new Exception("Invalid file format! for file :- "+fileName);
                }
                fileName = path+fileName;
                this.s3Adaptor.uploadFile(fileName, imgDto.getVideoFile(), true);
                imgDto.setVideoUrl(fileName);
            }

            if(imgDto.getThumbFile() != null){
                this.validateFile(imgDto.getThumbFile());
                String fileName = this.getFileName(imgDto.getThumbFile(), imgDto.getTitle());
                if(fileName.isEmpty()){
                    throw new Exception("Invalid file format! for file :- "+fileName);
                }
                fileName = path+fileName;
                this.s3Adaptor.uploadFile(fileName, imgDto.getThumbFile(), false);
                imgDto.setThumbUrl(fileName);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            logger.error("Exception while reading file errorMsg {}", ex.getMessage());
        }
    }

    public String getFileName(MultipartFile file, String title){
        String fileExt = this.getFileExtention(file.getOriginalFilename());
        String fileName = "";
        if (Arrays.stream(validFileExtensions).anyMatch(fileExt::equals)){
            fileName = title.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
            Date date = new Date();
            fileName = fileName + "_" + date.getTime() + "." + fileExt;
        }
        return fileName;
    }

    private boolean validateFile(MultipartFile file) throws Exception {
        boolean valid = true;

        if (!(file.getSize() > 0) || !file.getResource().isReadable()) {
            throw new Exception("Error Uploading file !.");
        }

        if (!this.isWebPImage(file) && !this.isVideoFile(file)){
            String[] imgExt = {"png", "jpeg", "jpg"};
            String fileExt = this.getFileExtention(file.getOriginalFilename());
            if(!Arrays.asList(imgExt).contains(fileExt)){
               throw new Exception("Invalid image type");
            }
        }
        return valid;
    }

    private String getFileExtention(String fileName){
        int i = fileName.lastIndexOf(".");
        String fileExt = fileName.substring(i + 1);
        return fileExt;
    }

    private Boolean isWebPImage(MultipartFile file){
        return file.getContentType() == IMAGETYPE_WEBP && this.getFileExtention(file.getOriginalFilename()) == IMAGE_EXT_WEBP;
    }

    private Boolean isVideoFile(MultipartFile file){
        return file.getContentType() == VIDEOTYPE_MP4 && this.getFileExtention(file.getOriginalFilename()) == VIDEO_EXT_MP4;
    }

    public Map<String, String> setMaskedData(ImageCreateRequestDto imgDto, Inspection insData, String utmSource){
        ReflectionUtil refUtil = new ReflectionUtil();
        Map<String, String> maskRespone = new HashMap<String, String>();
        Object section = refUtil.getObjAttribute(insData.getInspectionData(), imgDto.getSection(), true);
        if (imgDto.getType().equals(ConstantFields.AdditionalInfo)) {
            Object part =   refUtil.getObjAttribute(section, imgDto.getPart(), true);
            Object subPart = this.getSubPartForMsking(imgDto, part);
            Object partToUpdate = (subPart == null) ? part : subPart;
            ArrayList<AdditionalInfo> additionalInfoList = (ArrayList<AdditionalInfo>) refUtil.getObjAttribute(partToUpdate, imgDto.getType(), true);
            for(AdditionalInfo additionalInfo : additionalInfoList){
                if(additionalInfo.getHashKey().equals(imgDto.getHashKey())){
                    maskRespone = this.setImageMaskData(additionalInfo.image, imgDto, utmSource);
                }
            }
        } else {
            ArrayList<ImageWebModel> imgList =  (ArrayList<ImageWebModel>)refUtil.getObjAttribute(section, imgDto.getType(), true);
            for (ImageWebModel image : imgList) {
                if (image.getHashKey().equals(imgDto.getHashKey())){
                    maskRespone = this.setImageMaskData(image, imgDto, utmSource);
                }
            }
        }
        return maskRespone;
    }

    private Map<String, String> setImageMaskData(ImageWebModel image, ImageCreateRequestDto imgDto, String utmSource){
        Map<String, String> maskRespone = new HashMap<String, String>();
        String oldUrl = image.getUrl();
        String originalUrl = image.getOriginalUrl();
        image.setUrl(imgDto.getImageUrl());
        image.setMaskedBy(utmSource.toUpperCase());

        if (utmSource.equals(UTM_SOURCE_QC)) {
            image.setQc(new UtmSource(imgDto.getImageUrl()));
        } else if (utmSource.equals(UTM_SOURCE_CJEDIT)){
            image.setCjedit(new UtmSource(imgDto.getImageUrl()));
        }
        maskRespone.put("url", imgDto.getImageUrl());
        maskRespone.put("oldUrl", oldUrl);
        if (originalUrl !=null){
            image.setOriginalUrl(originalUrl);
        }
        return maskRespone;
    }

    private Object getSubPartForMsking(ImageCreateRequestDto imgDto, Object part){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object obj = null;
        if(imgDto.getSubPart() != null) {
            Object subParts = refUtil.getObjAttribute(part, "subParts", null);
            Object subpartsPart = refUtil.getObjAttribute(subParts, imgDto.getSubPart(), null);
            obj = subpartsPart;
            if (imgDto.getSubSubPart() != null) {
                Object subSubParts = refUtil.getObjAttribute(subpartsPart, "subSubPart", null);
                Object subSubPartsPart = refUtil.getObjAttribute(subSubParts, imgDto.getSubPart(), null);
                obj = subSubPartsPart;
            }
        }
        return obj;
    }

    public Boolean prepareAndpushPlateMaskingToDS(ImageCreateRequestDto imgDto, Inspection insData) {

        Boolean dataPushed = true;
        Map<String, Object> pushedMessage;
        if (insData.getVersion() ==0 && insData.getIsCritical() == null && configService.isPlateMaskingSingleRequestEnable(insData.getStoreId())) {
            final Query query = new Query();
            query.addCriteria(Criteria.where("appointmentId").is(insData.getAppointmentId()));
            query.addCriteria(Criteria.where("isRequestLocked").exists(true));
            InsPlateMaskingLog mskingLog = mongoAdapter.findOne(query, InsPlateMaskingLog.class);

            if (mskingLog != null){
                dataPushed =  false;
            }

            if (!IMAGE_HASHKEY_TEMPLATE.containsKey(imgDto.getHashKey())) {
                dataPushed =  false;
            }

            if (dataPushed) {
                pushedMessage = new HashMap<String, Object>() {{
                    put("id",   Uniqid.uniqid("",true)+""+DateUtil.getTime()+"-"+DateUtil.getTime()+DS_RESPONSE_TIME_OUT);
                    put("appointmentId" , insData.getAppointmentId());
                    put("storeId",  insData.getStoreId());
                    put("category", PLATE_MASKING_CATEGORY);
                    put("key", IMAGE_HASHKEY_TEMPLATE.get(imgDto.getHashKey()));
                    put("url", imgDto.getImageUrl());
                }};
                amqp.send(pushedMessage, DS_IMAGE_MASKING_ROUTING_KEY);
                this.savePlateMaskingLog(pushedMessage, insData.getAppointmentId());
            }
        } else {
            dataPushed = false;
        }
        return dataPushed;
    }

    public void savePlateMaskingLog(Map<String, Object> data, String apptId){
        final Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where("appointmentId").is(apptId));
        update.set("storeId", data.get("storeId"));
        update.set("flowType", 1);
        update.addToSet("request", new PlateMaskLog(data));
        mongoAdapter.findAndModify(query, update, FindAndModifyOptions.options().upsert(true), InsPlateMaskingLog.class);
        InsPlateMaskingLog mskingLog = mongoAdapter.findOne(query, InsPlateMaskingLog.class);
    }

    public void validateAuthorization(List<String> authorization) throws Exception {

        if (authorization == null){
            throw new Exception("Forbidden!-403");
        }
        String[] token = authorization.toString().split(" ");
        System.out.println(token);
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        Long time = currentTimestamp.getTime()/1000;

        //update Data
        final Query query = new Query();
        query.addCriteria(Criteria.where("access_token").is(StringUtils.chop(token[1])));
        OauthAccessTokens result = mongoAdapter.findOne(query, OauthAccessTokens.class);
        if(result == null) {
            throw new Exception("Token is invalid!-401");
        }
        if(time > result.getExpires()) {
            throw new Exception("Token is expired!-401");
        }
    }
}
