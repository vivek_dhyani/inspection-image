package com.cars24.inspection.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.cars24.inspection.entity.inspection.Inspection;
import org.springframework.stereotype.Repository;

@Repository
public interface InspectionRepository extends MongoRepository<Inspection, String> {

    Inspection findByCarId(Integer carId);

}
