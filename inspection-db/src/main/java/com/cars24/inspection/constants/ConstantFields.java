package com.cars24.inspection.constants;

public interface ConstantFields {

    String AdditionalInfo = "additionalInfo";
    String MainImages = "mainImages";
    String NoConnectionError = "Network Failure";
    String AuthFailureError = "Authentication Failure";
    String NetworkError = "Network Error";
    String ParseError = "Parse Error";
    String ServerError = "Server Error";
    String TimeoutError = "Timeout Error";

    int EVENT_RESET = 0;
    int EVENT_BACK = 1;

    // Prefrences Keys
    String DEVICE_REGISTER_KEY = "deviceRegisterResponse";
    String LOGIN_KEY = "login";
    String STORE_NAME_KEY = "store_name";
    String FILTER_STATE_KEY = "filter_param";
    int PROFILE_INSPECTOR = 0;
    int PROFILE_MANAGER = 1;
    int PROFILE_QC = 2;

    //bundle args
    String BUNDLE_SERIALIZABLE_DATA = "serializableData";
    String BUNDLE_IS_EDITABLE = "is editable";
    String BUNDLE_SEARCH_INSPECTION = "search_inspection";
    String BUNDLE_APPOINTMENT = "appointment";
    String BUNDLE_SCREEN_NAME = "screenName";
    String BUNDLE_MAX_TIME = "max_time";
    String BUNDLE_MIN_TIME = "min_time";
    String BUNDLE_CALENDER_TYPE = "calendar_type";
    String BUNDEL_TIMESTAMP = "timestamp";
    String BUNDLE_CITY_ASSEST = "assestCity";
    String BUNDLE_MAKE_ASSEST = "assestMake";
    String BUNDLE_MODEL_ASSEST = "assestModel";
    String BUNDLE_VARIANT_ASSEST = "assestVariant";
    String BUNDLE_VARIANT_YEAR_ASSEST = "assestVariantYear";
    String BUNDLE_STATE_ASSEST = "assestState";
    String BUNDLE_LOCATION_STORE_ASSEST = "assestLocationStore";
    String BUNDLE_COMMENTS_ASSEST = "assestComments";
    String BUNDLE_HIGHLIGHT_POSITIVES_ASSEST = "assestHighlightPositives";
    String BUNDLE_DEALERS_ASSEST = "assestDealers";
    String BUNDLE_RA_ASSEST = "assestRA";
    String BUNDLE_C2C_DEALERS_ASSEST = "assestC2CDealers";

    // SPINNER HINTS
    String VARIANT_HINT = "Variant";
    String VARIANT_YEAR_HINT = "Year";
    String IS_NEWAPPOINTMENT = "isBlank";

    // Tread Depth wheel spinner

    String[] TREAD_DEPTH_MM = {"0 - 1.6", "1.6 - 2", "2 - 3", "3 - 4", "4 - 5", "5 - 6", "6 - 7", "7+"};
    String[] PERCENTAGE = {"0%", "0-6%", "6-22%", "22-38%", "38-53%", "53-69%", "69-85%", "85-100%"};

    String AUTHORIZATION = "Authorization";
    /**
     * inspection Status Constants
     */
    String PENDING = "pending"; // Not use because It's automatically handled by API
    String NEW = "new";
    String REVIEW = "review";
    String QC = "qc";
    String DONE = "done";
    String UPLOAD_PENDING = "uploadPending";
    String UPLOAD_FAILED = "uploadFailed";
    String SM_REVIEW = "smReview";
    String QC_REVIEW = "qcReview";
    String SM_PENDING = "smPending";
    String QC_PENDING = "qcPending";
    String EMPTY_MESSAGE = "";
    // Rejected Messages
    String REVIEW_REJECTED = "reviewRejected";
    String QC_REJECTED = "qcRejected";
    String QC_REVIEW_REJECTED = "qcReviewRejected";
    String SM_REVIEW_REJECTED = "smReviewRejected";
    // RolesaCTION

    String INSPECTOR = "inspector";
    String STORE_MANAGER = "store manager";
    String SM = "sm";
    String STOREMANAGER = "storemanager";

    int[] ERROR = {401, 403, 400};

    //Alignment for Custom Dialog Text
    int ALIGN_LEFT = 1;

    int COMMENT_MAXLINES = 3;
    int COMMENT_MINLINES = 1;

    int NUM_OF_DAYS_BEFORE_PURGING = 7;
    int MAX_DAYS_BEFORE_PURGE = 10;

    String IS_PERMISSION_DIALOG_OPEN_ONCE = "permission_dialog";
    int NO_SELECTION = -1;
    String EMPTY = "";
    String FLOAT_EMPTY = "0.0";
    int CALENDAR_REGISTRATION_DATE = 210;

    String USER = "user";
    String APPOINTMENT_ID = "appointment_id";
    String APPOINTMENT_SEARCH = "search";
    String ROLE = "role";
    String STORE_ID = "store_id";
    String STORE_TYPE = "store_type";
    String ACTION_TYPE = "action_type";
    String PAGE_NO = "page_no";
    String LOCATION_ID = "location_id";
    String VERSION_CODE = "version_code";
    String VARIANT_ID = "variant_id";
    String VARIANT_YEAR = "variant_year";
    String STATUS_INACTIVE = "Inactive";
    String STATUS_ACTIVE = "Active";
    String CAR_ID = "carId";

    String WORK_TO_BE_DONE = "workToBeDone";
    String CURRENT_CONDITION = "currentCondition";
    String TREAD_DEPTH = "treadDepth";
    String WORK_DONE = "workDone";
    String PANEL_PAINT = "panelPaint";
    String FACTORY_SEALANT = "factorySealant";
    String IMAGE = "image";
    String VIDEO = "video";
    String IMAGE_CONFIG = "imageConfig";
    int MAX_INDEXED_IMAGES = 5;
    String CAR_DETAIL_RC = "rcImages";
    String CAR_DETAIL_WARRANTY = "warrantyImages";
    String CAR_DETAIL_INSURANCE = "insuranceImages";
    String CAR_DETAIL_HYPOTHECATION = "hypothecationImages";
    String BUNDLE_RECORDS = "records";
    int VIEW_VALIDATION_ERROR = 1; //1 Validation error
    int VIEW_REJECTED = VIEW_VALIDATION_ERROR + 1;// 2 Rejected
    int VIEW_NON_EDITABLE = VIEW_REJECTED + 1;// 3
    int VIEW_EDITABLE = VIEW_NON_EDITABLE + 1; //4 Editable/fixed
    String EXTERIORTYRES = "exterior-tyres";
    String ELECTRICALSINTERIOR = "electrical-interior";
    String ENGINETRANSMISSION = "engine-transmission";
    String STEERINGSUSPENSIONBRAKES = "steering-suspension";
    String AIRCONDITIONING = "air-condition";
    String CARDETAILS = "car-details";
    String SUMMARYTAB = "summary";
    String IMAGE_REVIEW_TAB = "IMAGE REVIEW";
    String PLEASE_SELECT = "Please select ";
    int TIME_OUT_CONNECTION = 60;
    int TIME_OUT_WRITE = 180;
    int TIME_OUT_READ = 180;
    String REJECTION_REMARK = "rejectionRemark";
    String ACCESS_TOKEN_RETRY_COUNT = "ACCESS_TOKEN_RETRY_COUNT";
    String ERRORS = "errors";
    String VALIDATION_ERRORS = "validationErrors";
    String RELOAD_HOME_UI = "RELOAD_HOME_UI";

    String RESULT_DATA = "RESULT_DATA";
    String MAX = "Max";
    String ACTION_VAHAN_SMS_RECEIVED = "action_vahan_sms_received";
    String COURTESY = "Courtesy";
    String CAR_DETAIL_REGISTRATION_NUMBER_KEY = "registrationNumber";
    String CAR_DETAIL_CHASSIS_NUMBER_KEY = "chassisNumber";
    String CAR_DETAIL_COMMENTS_KEY = "comments";
    String CAR_DETAIL_NO_CLAIM_BONUS_PERCENTAGE_KEY = "noClaimBonusPercentage";
    String DEALER_PRICING_LIST = "dealerListingPrice";
    String CAR_DETAIL_NO_CLAIM_BONUS_KEY = "noClaimBonus";

    // Additional Info
    String ADDITIONAL_INFO_YES = "additional_info_yes";
    String ADDITIONAL_INFO_YES_NO_SWITCH = "additional_info_yes_no_switch";
    String ADDITIONAL_INFO_TITLE = "additional info";

    String FCM_TOKEN = "fcm_token";
    String FCM_TOKEN_SYNCED = "fcm_token_synced";
    int UNREAD_NOTIFICATION = 0;
    int READ_NOTIFICATION = 1;
    long VIDEO_FILE_THRESHOLD_KB = 1000; // file size in KB till that video will not compressed more than that compression is required ;
    String EVENT_TYPE_LOGIN = "cars24_inspection_app_login_event";
    String EVENT_LOGIN = "inspection_login";
    String LNOD_FILE_URL = "https://s3.ap-south-1.amazonaws.com/android-lnod/inspection-lnod/Cars24-Inspection-LNODRELEASE.apk";
    String ACTION_COM_VAHAN_SMS_RESULT = "com.cars24.inspection.action.vahan_sms_result";
    String VAHAN_APP_PACKAGE = "com.cars24.vahanapp";
    String FRANCHISEE_STORE = "franchise";

    String IS_SCREEN_ANALYTICS_ENABLED = "IS_SCREEN_ANALYTICS_ENABLED";

    String EVENT_TYPE_NUMBER_SELECTION = "cars24_inspection_app_number_selection";
    String EVENT_SIM_INFO = "sim_info";
    String EVENT_SIM_SELECTION_SCREEN_OPEN = "sim_selection_screen_open";
    String EVENT_SIM_INFO_CONFIG = "sim_info_config";
    String EVENT_SIM_SELECTION_UPDATE = "sim_selection_update";
    String EVENT_PHONE_READ_PERMISSION = "phone_read_permission";
    String SIM_CONFIG_VALUE = "SIM_CONFIG_VALUE";
    String COATING_DELETE_COMMAND = "$DELDAT-ALL#";
    String COATING_GET_ALL_COMMAND = "GETDAT-ALL#";
    CharSequence Invalid_Entry = "Insert correct entry";

    // store level config keys starts here
    String STORE_CONFIG_CUSTOM_CAM = "customCamera";
    String STORE_CONFIG_PAINT_COATING_METER = "paintCoatingMeter";
    String STORE_CONFIG_RECT_MARKER = "rectDamageMarker";
    String STORE_CONFIG_RFC = "rfc";
    String STORE_CONFIG_DAMAGE_MARKER_SEPARATE = "damageMarkerSeparate";
    String STORE_CONFIG_DISTANCE_MATRIX = "distanceMatrix";
    String STORE_CONFIG_NUMBER_PLATE_MASK = "numberPlateMask";
    String STORE_CONFIG_PREFILL_VAHAN_DATA = "vahanPrefilling";
    String STORE_CONFIG_FIELD_ALIAS_VALUE = "fieldAliasValue";
    String APPT_STORE_CONFIG_APPT_ASSIGNED = "appointmentAssigned";
    String APPT_STORE_CONFIG_EXT_OVER_REPORT = "exteriorOverReporting";
    String ERROR_MESSAGE_SAVE_LAST_VIEW = "Please save your marker first";
    int COATING_READING_MAX = 6;
    int GPS_REQUEST = 1005;
    String ENGINE_SOUND_FIELD = "engineSound";
    String EXHAUST_SMOKE_FIELD = "exhaustSmoke";
    String BUNDLE_RI_REJECTION = "isRiRejection";
    String BUNDLE_PANEL_PAINT = "isPanelPaint";
    String BUNDLE_IS_CR_FLOW = "isCRFlow";
    String BUNDLE_IS_REJECTED = "isRejected";
    String VIN_LAST_FIVE_CHAR_MASK = "*****";
    String INSURANCE_TYPE_COMPREHENSIVE = "Comprehensive";
    String INSURANCE_TYPE_ZERO_DEPRECIATION = "Zero Depreciation";
    String INSURANCE_TYPE_THIRD_PARTY = "3rd Party";
    String NO_MISMATCH = "No Mismatch";
    String DAMAGE_MARKER_TITLE = "Damage Marker";
    String ROAD_TAX_PAID_OTT = "OTT";
    String ROAD_TAX_PAID_LTT = "LTT";
    String ROAD_TAX_PAID_OTT_LTT = "OTT/LTT";
    String NO = "No";
    String YES = "Yes";
    String NA = "N/A";
    String MAKE_MODEL = "Make/Model";
    String ENGINE_SOUND_SCALE_MINOR = "Minor Sound";
    String ENGINE_SOUND_SCALE_MAJOR = "Major Sound";
    String ENGINE_SOUND_SCALE_CRITICAL = "Critical Sound";
    String ENGINE_SOUND_SCALE = "engineSoundScale";
    String BUNDLE_ENGINE_SOUND_SCALE = "engineSoundScale";
    String ENGINE_VIBRATION = "Engine Vibration";
    String ZERO = "0";

    String AIRBAG_NUMBER_TITLE = "No. Of Airbags";
    String FILE_NAME = "fileName";
    String ENGINE_SOUND_VIDEO = "engineSoundVideo";
    String Ok = "Ok";


    String BASE_THUMB_SIZE = "200x200/";
    String BASE_URL_SIZE = "590x445/";
    int RESPONSE_CODE_SERVER_VALIDATION = 412;
    int RESPONSE_CODE_RE_INSPECTION_FAILED = 1015;
    int INS_MISS_SERVER_VALIDATION_STATUS = 422;
    int REDUNDANT_FINAL_SUBMISSION = 1002;
    int NO_ANY_IMAGES_SYNCED = 1;
    int MEDIA_IS_SYNCING = NO_ANY_IMAGES_SYNCED + 1;
    int MEDIA_SYNC_ERROR = MEDIA_IS_SYNCING + 1;
    int SYNCED_ALL_MEDIA = MEDIA_SYNC_ERROR + 1;

    String PROFILE = "PROFILE";

    String VAHAN_SMS_STATUS_SENT = "SMS_SENT";
    String VAHAN_SMS_STATUS_NOT_SENT = "SMS_NOT_SENT";
    String VAHAN_SMS_STATUS_RESPONSE_RECEIVED = "SMS_RESPONSE_RECEIVED";
    String VAHAN_SMS_STATUS_RESPONSE_SYNCED = "SMS_RESPONSE_SYNCED";

    //build flavour
    String PROD = "prod";
    String STAGING = "staging";
    String QA4 = "qa4";
    String QA3 = "qa3";

    int VIDEO_UPLOAD_LIMIT_IN_SEC = 20;
    int MIN_REGISTRATION_YEAR = 1990;
    String SELECTED_MOB_NUM = "selectedMobNum";
    String SELECTED_SIM_SLOT = "selectedSIMSlot";
    String PATH = "path";
    String SIM_COUNT = "simCount";
    String READ_PHONE_PERMISSION = "readPhonePermission";
    String SIM_SLOT = "simSlot";
    String IS_EDIT_NUM = "editNumber";
    String IMAGE_FIELD = "imageField";
    String IS_REG_NO_EDITED = "isRegNoEdited";

    String IDLE = "Idle";
    String ACCELERATED = "Accelerated";
    String EXHAUST_SMOKE_VIDEO = "exhaustSmokeVideo";
    String EXHAUST_SMOKE = "Exhaust Smoke";
    String ENGINE_SOUND_IDLE_FIELD = "engineSoundVideoIdle";
    String ENGINE_SOUND_ACCELERATED_FIELD = "engineSoundVideoAccelerated";

    // file name record path
    String FILENAME_RECORD_PURGE_IMAGES = "inapp_purge_record.data";

    String FILENAME_RECORD_IMAGES_REQUESTED_FOR_UPLOAD = "inapp_upload_record.data";

    // Image sync state
    int IMAGE_SYNC_STATE_NOT_STARTED = 0x00100; //256
    int IMAGE_SYNC_STATE_SYNCING = IMAGE_SYNC_STATE_NOT_STARTED + 1; //257
    int IMAGE_SYNC_STATE_SUCCESS = IMAGE_SYNC_STATE_SYNCING + 1; //258
    int IMAGE_SYNC_STATE_ERROR = IMAGE_SYNC_STATE_SUCCESS + 1; //259
    int IMAGE_SYNC_STATE_COMPRESSION_NOT_DONE = IMAGE_SYNC_STATE_ERROR + 1; //260
    int IMAGE_SYNC_STATE_COMPRESSION_STARTED = IMAGE_SYNC_STATE_COMPRESSION_NOT_DONE + 1; //261
    int IMAGE_SYNC_STATE_COMPRESSION_DONE = IMAGE_SYNC_STATE_COMPRESSION_STARTED + 1; //262
    int IMAGE_SYNC_STATE_COMPRESSION_ERROR = IMAGE_SYNC_STATE_COMPRESSION_DONE + 1; //263

    int IMAGE_STATE_NOT_DELETING = 101;
    int IMAGE_STATE_DELETING = IMAGE_STATE_NOT_DELETING + 1;
    int IMAGE_STATE_DELETED = IMAGE_STATE_DELETING + 1;

    int COMPRESSED_IMAGE_WIDTH = 1450;
    int COMPRESSED_IMAGE_HEIGHT = 900;
    int COMPRESSED_IMAGE_QUALITY = 90;

    String IMAGE_COMPRESSION_DEFAULT_ALGO = "JPEG";
    String IMAGE_COMPRESS_TYPE_WEBP = "webp";
    float THRESHOLD_VALUE_STORAGE = 50; // storage in MB to show alert for low storage
    String ACTION_IMAGE_UPLOAD = "ACTION_IMAGE_UPLOAD";
    String ACTION_IMAGE_UPLOAD_OFFLINE = "ACTION_IMAGE_UPLOAD_OFFLINE";
    String IMAGE_UPLOAD_STATUS = "IMAGE_UPLOAD_STATUS";

    String CHASSIS_NUMBER_IMAGE_HASH_KEY = "inspectionData_cardetails_additionalImages_ChassisNumber";
    String FLASH_STATE = "flash_state";
    String JSON_PARSE_ERROR_SERVER_VALIDATION = "Parse error during server validation";
    int HTTP_RESPONSE_UNAUTHORIZED = 401;

    String INSURANCE_PDF_DIR = "insurancePdfFiles";
    String RI_REASON_DELIVERY_PAYMENT = "Delivery payment";
    String RC_AVAILABILITY_PHOTOCOPY = "Photocopy";
    String[] EXT_TYRE_FIELDS_FOR_MARKER = {"bonnetHood", "fenders", "doors", "quarterPanel", "dickyDoorBootDoor"};
    String SURVEY_URL = "SURVEY_URL";
    String ADD_CATEGORY = "Add Category";
    String HASH_KEY = "hashKey";
    String REQUEST_ID = "requestId";
    String GOOGLE_DISTANCE_MATRIX_ALGO = "googleDistanceMatrix";
    String WEB_VIEW_URL = "web_view_url";
    String CR_WAIVER_FORM_URL = "cr_waiver_form_url";

    int VIEW_TYPE_CELL_ITEM = 1;
    int VIEW_TYPE_FOOTER = 2;
    String VARIANT_NUDGE = "Variant nudge";

    String INS_ACTION_START = "start";
    String INS_ACTION_DENIED = "denied";
    String OTHERS = "Others";
    String USER_ID = "userId";
    String USER_STORE_ID = "userStoreId";
    String USER_EMAIL = "userEmail";
    String HOME_INSPECTION = "home-inspection";
    String ODOMETER_TAMPERED = "Odometer Tampered";
    String INSURANCE_EXPIRED = "Insurance Expired";

    String ALIAS_YES_NO_WORK = "No Work";
    String ALIAS_YES_ONLY_RUBBNG = "Only Rubbing/Polishing required";
    String ALIAS_NO_REQUIRE_REFURBISHMENT = "Require refurbishment";
    String ALIAS_NA = "N/A";
    String CR_QC_IMAGE_MASK_MSG = "Corrected by QC P.E. Reg number visible";
    String FIREBASE_DB_QA = "inspection-qa";
    String FIREBASE_DB_STAGE = "inspection-stage";
    String FIREBASE_DB_PROD = "inspection-prod";
    String NO_OF_PANELS = "noOfPanels";
    String CHANCE_OF_ACHIEVING_TARGET_PRICE = "chancesOfachievingTargetPrice";

    String SEVERITY_MAJOR = "Major";
    String SEVERITY_SEVERE = "Severe";
}
