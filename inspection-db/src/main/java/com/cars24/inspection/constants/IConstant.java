package com.cars24.inspection.constants;

public interface IConstant extends ConstantFields {

    interface BundleKey {
        String ADDITIONAL_INFO = "additionalInfo";
        String TITLE = "title";
        String SUBPARTS = "subparts";
        String SUBSUBPARTS = "subSubparts";
        String PARTS = "PARTS";
        String TYPE = "type";
        String BASEPOJO = "basepojo";
        String VALUES = "values";
        String POSITION = "position";
        String IMAGE_RESPONSE = "image_response";
        String TAB = "tab";
        String RULE = "rule";
        String IMAGE_RESPONSE_LIST = "image_response_list";
        String BASEPOJOSUBPART = "basepojo_subpart";
        String PARENTFIELDS = "parent_fields";
        String TAB_STATE = "tab_state";
        String PARENT_POS = "parentPos";
        String CHILD_POS = "childPos";
        String IMAGE_TYPE = "imageType";
        String VIDEO_TYPE = "videoType";
        String OFFLINE = "offline";
        String SYNC_ALL_OFFLINE = "sync_all";
        String CHILD_POSITION = "child_position";
        String REPORT_SUBMIT = "report_submit";
        String HIGHLIGHTED_POS = "highlighted_pos";
        String SHOW_VALIDATION_ERROR = "showValidationError";
        String INSPECTION_MISS = "inspectionMiss";
        String SUBMIT_INS_MISS = "submit_ins_miss";
        String INSPECTION_MISS_COMMENTS = "inspectionMissComments";
        String APPOINTMENT_ID = "appointmentId";
        String INS_MISS_REQUEST_ID = "requestId";
        String PATH = "path";
        String PROFILE_APPT = "profile_appointment";
        String INCENTIVE_PDF = "incentive_pdf";
        String EMAIL = "email";
        String VIRTUAL_NUMBER = "phone";
        String INSPECTION_CAMERA = "inspection_camera";
        String LOCATION_LAT = "latitude";
        String LOCATION_LNG = "longitude";
        String REMARK_HISTORY = "remark_history";
        String FROM_CR_PROFILE = "from_cr_profile";
        String DOCUMENT_UPLOAD = "document_upload";
        String FILE_PATH = "fileLoc";
        String INS_END_TIME = "end_time";
        String INSPECTION_MODEL = "inspection_model";
        String PARAMS = "inspection_model";
        String UPLOAD_TIME = "uploadTime";
        String MONTH = "month";
        String YEAR = "year";
        String DAY = "day";
        String NOTIFICATIONS = "notifications";
        String SELECTED_NOTIFICATIONS = "selected_notifications";
        String CAMERA_MODE = "CAMERA_MODE";
        String SELECTED_IMAGE_PATH = "SELECTED_IMAGE_PATH";
        String APPT_VERSION = "appt_version";
        String SECTION = "section";
        String RI_REASON_ID = "ri_reason_id";
        String RI_REASON = "ri_reason";
        String SMS_MESSAGE = "sms_message";
        String POINTER_COUNT = "pointerCount";
        String BUNDLE_VAHAN_OBJ = "VAHAN_MODEL";
        String BUNDLE_DEVICE_NAME = "DEVICE_NAME";
        String BUNDLE_DEVICE_MAC = "DEVICE_MAC";
        String COATING_METER_MAC = "COATING_METER_MAC";
        String COATING_METER_NAME = "COATING_METER_NAME";
        String IMAGE_IS_TILTED = "isTilted";
        String FIELD = "Field";
        String CHANGE_PASSWORD = "change_password";
        String SELECTED_STORE_CITY_ID = "store_city_id";
        String DS_RATING_DATA = "ds_rating";
        String RATING_CHANGES = "rating_changes";
        String RATING_DIALOG_TITLE = "rating_dialog_title";
        String RATING_CHANGE_FIELD = "rating_change_field";
        String REGISTRATION_MONTH = "registrationMonth";
        String VIEW_STATE = "VIEW_STATE";
        String INURANCE_COMPANY_NAME = "insurance_company_name";
        String INURANCE_COMPANY_TYPE = "insurance_company_type";
        String APP_DOWNLOAD_URL = "appDownloadUrl";
        String IS_DOWNLOADED = "isDownloaded";
        String COMPRESSION_STATE = "Compression_State";
        String MARKER_IMAGE_PATH = "marker_image_path";
        String MARKER_LIST = "marker_list";
        String IS_MARKER_IMAGE = "isMarkerImage";
        String B2C_DEALER_ID = "b2cDealerId";
        String IS_NO_YES_SWITCH = "isNoYesSwitch";
        String IS_FROM_INS_MIS = "isFromInsMis";
        String SELECTED_VALUE = "selectedValue";
        String IS_NOTIFICATION_CLICKED = "isTrigger";
        String NOTIFICATION_INS_STATUS = "notification_ins_status";
        String NOTIFICATION_INS_VERSION = "notification_ins_version";
        String NOTIFICATION_INS_TIME = "notification_ins_time";
        String NOTIFICATION_TIME = "notification_time";
        String START_INSPECTION = "start_inspection";
        String FIELD_KEY = "fieldKey";
        String TAB_INDEX = "tabIndex";
        String EXTERIOR_TAB = "exteriorTab";
        String EXTERIOR_UI_MODEL = "exteriorUiModel";
        String IMAGE_ID = "imageId";
        String BLUR_SCORE = "blurScore";
        String SUB_TITLE = "subTitle";
        String CR_WAIVE_STATUS = "crWaiveOffStatus";
        String CR_QC_COMMENT = "crQcComment";
        String CATEGORY = "category";
        String SUB_CATEGORY = "subCategory";
        String VAHAN_MSG = "vahanMsg";
        String NO_OF_OWNERS_IN_VAHAN_MSG = "noOfOwnerInVahanMag";
        String NUMBER_OF_OWNERS = "noOfOwners";
        String NUDGE_NAME = "noOfOwners";
        String STORE_ID = "storeId";
        String LAT = "lat";
        String LNG = "lng";
        String START_DENY_ACTION = "start_deny_action";
        String ASSIGNED_COUNT = "assignedCount";
        String TODAY_TOMORROW = "todayTomorrow";
        String FROM_NOTIFICATION = "fromNotification";
        String APPT_DATE = "apptDate";
        String BIKE_APPT_DTO = "create_bike_appt_dto";
        String INSPECTED_CAR_MSG = "alreadyInspected";
        String LAST_ODOMETER_READING = "lastOdometerReading";
        String CURR_ODOMETER_READING = "currOdometerReading";
        String NUDGE_TO_SHOW = "nudgeToShow";
        String SUGGESTED_VARIANT_IDS = "suggestedVariantIds";
        String APPROVAL_MSG = "ra_approval_msg";
        String TOTAL_BAD_PANEL = "total_bad_panels";
        String LOW_CHANCE_PERCENTAGE = "low_chance_of_percentage";

    }

    interface Bundle {
        String NEW_APPOINTMENT = "newAppointment";
        String EVENT = "event";
        String TITLE = "title";
        String REGISTRATION_CITY = "registrationCity";
        String YEAR = "year";
        String ASSIGNED_LOCATION_ID = "assigned_location_id";
    }

    interface CommentsType {
        String Comments = "Comments";
    }

    interface VideoType {
        String MAIN_VIDEO = "mainVideos";
    }

    interface VideoField {
        String INSPECTIONDATA_EXTERIORTYRES_VIDEO = "inspectionData_exteriorTyres_video";
    }

    interface AppointmentTypeParam {
        String CRITICAL_DETAIL = "critical-detail";
        String NON_CRITICAL_DETAIL = "non-critical-detail";
    }

    interface AppointmentType {
        String CRITICAL_REJECT = "critical-list";
        String NON_CRITICAL_REJECT = "non-critical-list";
        String INSPECTION_MISS = "inspection-miss";
    }

    interface NotificationType {
        String CRITICAL_REJECT = "critical_reject";
        String NON_CRITICAL_REJECT = "non_critical_reject";
        String INSPECTION_MISS = "inspection_miss";
        String CRITICAL_REJECT_CAP = "CRITICAL_REJECT";
        String NON_CRITICAL_REJECT_CAP = "NON_CRITICAL_REJECT";
        String HOME_INSPECTION = "HOME_INSPECTION";
        String SCHEDULED = "SCHEDULED";
        String CR_WAIVER = "CR_WAIVER";
        String HOME_INSPECTION_NEAR_BY = "HOME_INSPECTION_NEAR_BY";
    }

    interface Section {
        String cd = "carDetails";
        String extTyre = "exteriorTyres";
        String ei = "electricalsInterior";
        String et = "engineTransmission";
        String ssb = "steeringSuspensionBrakes";
        String ac = "airConditioning";
        String summary = "summary";
        String imageReview = "ImageReview";
    }

    interface HeaderType {
        String FILTER_SCREEN = "filter_screen";
        String SECTION_SCREEN = "SECTION_SCREEN";
        String ADDITIONAL_INFO_SCREEN = "additional_Info_screen";
        String COMMENTS_SCREEN = "comments_screen";
        String DEFAULT_SCREEN = "default_screen";
    }

    //Store types
    interface StoreType {
        String B2B = "B2B";
        String B2C = "B2C";
        String PNS = "PnS";
        String LOCALITY = "locality";
    }

    interface DatePart {
        String MONTH = "month";
        String YEAR = "year";
        String DAY = "day";
    }

    interface FieldsKey {
        String ODOMETER_READING = "odometerReading";
        String REGISTRATION_YEAR = "registrationYear";
        String MANUFACTURING_YEAR = "year";
        String FITNESS_UPTO = "fitnessUpto";
        String MODEL = "model";
        String RA_DETAILS = "raDetails";
        String REGISTRATION_CITY = "registrationCity";
        String REGISTRATION_STATE = "registrationState";
        String STORE_NAME = "storeName";
        String CHASSIS_NUMBER_EMOBOSSING = "chassisNumberEmbossing";
        String RC_AVAILABILITY = "rcAvailability";
        String INSURANCE_TYPE = "insuranceType";
        String FUEL_TYPE = "fuelType";
        String ADDITIONAL_IMAGES = "additionalImages";
        String CITY = "city";
        String MAKE = "make";
        String VARIANT = "variant";
        String RTO = "rto";
        String MISMATCH_IN_INSURANCE = "mismatchInInsurance";
        String MISMATCH_IN_RC = "mismatchInRC";
        String RC_CONDITION = "rcCondition";
        String ORIGINAL_INVOICE = "originalInvoice";
        String APPOINTMENT_ID = "appointmentId";
        String CHASSIS_NUMBER_IMAGE = "chassisNumberImage";
        String RC_IMAGE = "rcImages";
        String WARRANTY_IMAGE = "warrantyImages";
        String INSURANCE_IMAGE = "insuranceImages";
        String TO_BE_SCRAPPED = "toBeScrapped";
        String INSURANCE_EXPIRY_DATE = "insuranceExpiry";
        String NO_CLAIM_BONUS = "noClaimBonus";
        String NO_CLAIM_BONUS_PERCENTAGE = "noClaimBonusPercentage";
        String ALLOY_WHEELS = "alloyWheels";
        String ORVM = "ORVM";
        String BLOCK_DEALERS = "blockedDealers";
        String DEALER_LISTING_PRICE = "dealerListingPrice";
        String POWER_WINDOWS_NUMBER = "powerWindowsNumber";
        String AIRBAG_FEATURE = "airbagFeature";
        String NUMBER_OF_AIRBAGS = "numberOfAirbags";
        String POWER_WINDOWS = "powerWindows";
        String AUTHORIZED_STORE = "arStores";
        String PANEL_PAINT = "panelPaint";
        String ENGINE_BLOW_BY = "engineBlowBy";
        String IS_PROPER_CONDITION = "inProperCondition";
        String MODEL_ID = "modelId";
        String ENGINE_OIL = "engineOil";
        String ENGINE_CAPACITY = "engineCapacity";
        String WOODEN_COLOR_INTERIOR = "woodenColoredInterior";
        String ABS = "abs";
        String PUSH_START_BUTTON = "pushStartButton";
        String AUTOMATIC_TARNSMISSION = "automaticTransmission";
        String CLIMATE_CONATROL_AC = "climateControlAC";
        String ENGINE_MOUNTING = "engineMounting";
        String ENGINE_VIBRATION = "engineVibration";
        String ROAD_TAX_PAID = "roadTaxPaid";
        String RC_AT_DELIVERY = "rcAtDelivery";
        String CHASSIS_NUMBER_ON_RC = "chassisNumberOnRC";
        String LIGHTS = "lights";
        String RTO_NOC_VALIDITY = "rtoNocValidity";
        String RTO_NOC_ISSUED_DATE = "rtoNocIssuedDate";
        String DUPLICATE_KEY = "duplicateKey";
        String DUPLICATE_KEY_IMAGE = "duplicateKeyImage";
        String EXTENDED_WARRANTY_EXPIRY = "extendedWarrantyExpiryDate";
        String NO_OF_OWNER_TITLE = "No. Of Owner";
        String FINANCIER_NAME = "financierName";
        String IS_UNDER_HYPOTHECATION = "isUnderHypothecation";
    }

    interface ACTIVITY_REQUEST_CODE {
        int MAIN_IMAGE_CAPTURE_REQUEST = 100 + 1;
        int GOOGLE_SIGN_IN = MAIN_IMAGE_CAPTURE_REQUEST + 1;
    }

    /**
     * Fuel type values for ui
     */
    interface FuelTypeUiValue {
        String PETROL = "Petrol";
        String DIESEL = "Diesel";
        String PETROL_CNG = "Petrol + CNG";
        String PETROL_LPG = "Petrol + LPG";
        String ELECTRIC = "Electric";
        String HYBRID = "Hybrid";
    }

    /**
     * Fuel Type distinctValue from DB table
     * Diesel
     * Petrol
     * CNG
     * LPG
     * Electric
     * Hybrid
     */
    interface FuelTypeDBValue {
        String PETROL = "Petrol";
        String DIESEL = "Diesel";
        String PETROL_CNG = "CNG";
        String PETROL_LPG = "LPG";
        String ELECTRIC = "Electric";
        String HYBRID = "Hybrid";
    }

    interface CarDetailsTAB {
        String APPOINTMENT = "appointment";
        String RC = "rc";
        String WARRANTY = "warranty";
        String INSURANCE = "insurance";
        String CAR_DETAILS = "car_details";
        String CHASSIS = "chassis";
    }

    interface TAB {
        int APPOINTMENT = 0;
        int RC_INFO = APPOINTMENT + 1;
        int CAR_INFO = RC_INFO + 1;
        int INSURANCE = CAR_INFO + 1;
        int CHASSIS_NUMBER = INSURANCE + 1;
    }

    /**
     * Old Inspection data Current conditions for field Engine permission blow by
     * JIRA # INS-2853
     */
    interface OldCCValues {
        String NoBlowBy = "NO Blow-by";
        String PermBlowByonidle = "Perm. Blow By on idle";
        String OilSpillageOnIdle = "Oil spillage on Idle";
        String PermBlowbyOilSpillageOnIdle = "Perm. blow by & oil spillage on idle";
        String BATTERY_NOT_WORKING = "Not Working";
        String BATTERY_Side_walls_bulge = "Side walls bulge";
        String BATTERY_TOP_COVER_DAMAGED = "Top cover damaged";
        String BATTERY_FLUX_ON_TERMINAL = "Flux on Terminal";
        String CLUTCH_SLIP = "Slip";
        String CLUTCH_LOW_PICKUP = "Low Pick Up";
    }

    /**
     * New Inspection data Current conditions for field Engine permission blow by
     * JIRA # INS-2853
     */
    interface NewCCValues {
        String NoBlowBy = "No blow-by";
        String PermBlowByonidle = "Perm. blow-by on idle";
        String OilSpillageOnIdle = "Oil spillage on idle";
        String PermBlowbyOilSpillageOnIdle = "Perm. blow-by & oil spillage on idle";
        String BATTERY_DEAD = "Dead";
        String BATTERY_DAMAGED = "Damaged";
        String CLUTCH_SLIP_LOW_PICKUP = "Slip/ Low Pick Up";
        String BATTERY_NOT_AVAILABLE = "Battery not available";
        String COOLANT_MIXED_WITH_ENGINE_OIL= "Coolant mixed with engine oil";
    }

    interface additionalInfoTitle {
        String BLOW_BY_PERMISSIBLE = "Engine Permissible Blow by (Back Compression)";
        String STEERING_MOUNTED = "Steering Mounted Audio Control";
        String DICKY_DOOR = "Dicky Door / Boot Door";
        String LOWER_CROSS_MEMBER = "Lower Cross Member";
        String UPPER_CROSS_MEMBER = "Upper Cross Member (Bonnet Patti)";
        String ORVM = "ORVM - Manual/Electrical";
    }

    /**
     * Main Image Field key
     */
    interface MainImageField {
        //Exterior Tyre start here
        String INSPECTIONDATA_EXTERIORTYRES_FRONTMAIN = "inspectionData_exteriorTyres_mainImages_frontMain";
        String INSPECTIONDATA_EXTERIORTYRES_FRONTWITHHOODOPEN = "inspectionData_exteriortyres_mainImages_frontWithHoodOpen";
        String INSPECTIONDATA_EXTERIORTYRES_LEFTHANDSIDE = "inspectionData_exteriorTyres_mainImages_FrontLeftSide";
        String INSPECTIONDATA_EXTERIORTYRES_LEFTSIDE = "inspectionData_exteriortyres_mainImages_sideLeft";
        String INSPECTIONDATA_EXTERIORTYRES_REARLEFTSIDE = "inspectionData_exteriortyres_mainImages_rearLeftSide";
        String INSPECTIONDATA_EXTERIORTYRES_REARMAIN = "inspectionData_exteriortyres_mainImages_rearMain";
        String INSPECTIONDATA_EXTERIORTYRES_FRONTRIGHTSIDE = "inspectionData_exteriortyres_mainImages_frontRightSide";
        String INSPECTIONDATA_EXTERIORTYRES_RIGHTSIDE = "inspectionData_exteriortyres_mainImages_sideRight";
        String INSPECTIONDATA_EXTERIORTYRES_REARRIGHTSIDE = "inspectionData_exteriorTyres_mainImages_RearRightSide";
        String INSPECTIONDATA_EXTERIORTYRES_REARBOOTOPENED = "inspectionData_exteriortyres_mainImages_rearBootOpened";
        String INSPECTIONDATA_EXTERIORTYRES_REARBOOTOPENEDWITHSPARETYRE = "inspectionData_exteriortyres_mainImages_rearBootOpenedWithSpareTyre";
        //ELECTRICAL INTERIOR start here
        String INSPECTIONDATA_ELECTRICALINTERIOR_ODOMETER = "inspectionData_electricalInterior_mainImages_odometer";
        String INSPECTIONDATA_ELECTRICALINTERIOR_INTERIORFROMBACK = "inspectionData_electricalInterior_mainImages_interiorfromback";
        String INSPECTIONDATA_ELECTRICALINTERIOR_INTERIORFROMFRONTRIGHT = "inspectionData_electricalInterior_mainImages_interiorfromfrontright";
        String INSPECTIONDATA_ELECTRICALINTERIOR_INTERIORFROMREARRIGHT = "inspectionData_electricalInterior_mainImages_interiorfromrearright";
        // ELECTRICAL INTERIOR ends here
        String INSPECTIONDATA_ENGINETRANSMISSION_ENGINEBLOWBY = "inspectionData_enginetransmission_engineblowby";
        String INSPECTIONDATA_ENGINETRANSMISSION_EXHAUSTCOLORSMOKE = "inspectionData_enginetransmission_exhaustcolorsmoke";
    }

    /**
     * AdditionalImage field key
     */
    interface AdditionImagesField {
        String INSPECTIONDATA_CARDETAILS_CHASSIS_NUMBER = "inspectionData_cardetails_additionalImages_ChassisNumber";
    }

    /**
     * Image types
     * Common screen like MainImage capture or Custom cam capture use this
     */
    interface ImageType {
        String MAIN_IMAGE = "mainImages";
        String ADDITIONAL_INFO_IMAGE = "additionalInfo";
        String NORMAL_IMAGE = "normal";
        String WARRANTY_IMAGE = "warrantyImages";
        String RC_IMAGE = "rcImages";
        String INSURANCE_IMAGE = "insuranceImages";
        String HYPOTHECATION_IMAGE = "hypothecationImages";
        String ADDITIONAL_IMAGES = "additionalImages";
        String DOCUMENT_IMAGES = "documentImages";
        String CHASSIS_NUMBER_IMAGE = "chassisNumberImage";
    }

    interface MediaType {
        String IMAGES = "image";
        String VIDEOS = "video";
    }

    interface Severity {
        String SCRATCHED = "scratched";
        String DAMAGED = "damaged";
        String DENTED = "dented";
        String RUSTED = "rusted";
    }

    interface VideoRecordingState {
        int VIDEO_RECORDING_NOT_STARTED = 0; // default state when recording is not yet started.
        int VIDEO_RECORDING = 1; // when recording is currently going on.
        int VIDEO_RECORDING_PAUSE = 2; // when recording is paused.
    }

    interface ExteriorTabs {
        String FRONT = "front";
        String LEFT = "left";
        String REAR = "rear";
        String RIGHT = "right";
    }

    // Grills shape of Hyundai Verna as per year variant
    interface GrillShape {
        String VGT = "VGT";
        String TRANSFORM = "TRANSFORM";
        String FLUIDIC = "FLUIDIC";
        String FLUIDIC_4S = "FLUIDIC 4S";
        String VERNA_2017_2020 = "VERNA 2017-20";
        String VERNA_2020 = "VERNA 2020";
    }
    // Engine Capacity Hyundai Verna as per year variant
    interface Capacity {
        String LITER_1_4 = "1.4 Liter";
        String LITER_1_6 = "1.6 Liter";
    }

    interface DialogTag{
        String REPORT_SUBMITTED = "report_submitted";
    }

    interface HourGlass {
        int NOT_STARTED = 0;
        int STARTED = NOT_STARTED + 1;
        int COMPLETED = STARTED + 1;
    }

    interface NudgeType {
        String NUDGE_OWNER_NUMBER = "nudgeOwnerNumber";
        String NUDGE_PAINT_COATING = "nudgePaintCoating";
        String NUDGE_HI_ASSIGNED_USER = "nudgeHIAssignedUser";
        String NUDGE_ENGINE_OVER_REPORTING = "nudgeEngineOverReporting";
        String NUDGE_ODOMETER = "nudgeOdometer";
        String NUDGE_INSURANCE_EXPIRED = "nudgeInsuranceExpired";
        String NUDGE_SUGGESTED_VARIANT = "nudgeSuggestedVariant";
        String NUDGE_BAD_PANEL_RA_APPROVAL = "nudgeBadPanelRAApproval";
    }

    interface NudgeName {
        String OWNER_NUMBER = "NUDGE OWNER NUMBER";
        String PAINT_COATING = "NUDGE PAINT COATING";
        String ENGINE_OVER_REPORTING = "NUDGE ENGINE OVER REPORTING";
        String HI_CALL = "HI_CALL";
        String HI_NOT_ASSIGNED_USER = "HI NOT ASSIGNED TO USER";
        String ODOMETER = "NUDGE ODOMETER";
    }

    interface InspectionHome{
        String CD = "CAR DETAILS";
    }

    interface EngineTransmissionCCWDToComments{
        //Engine related
        String ENGINE_CC_RPM_NOT_INCREASING = "RPM not increasing";
        String ENGINE_CC_SEIZED = "Seized";
        String ENGINE_CC_NOT_CONVERTING_TO_CNG = "Not converting to CNG";
        String ENGINE_CC_TURBO_CHARGE_NOT_WORKING = "Turbo charger not working";
        String ENGINE_CC_SUMP_DAMAGED = "Sump Damaged";
        String Engine_CC_CAR_NOT_WORKING_ON_PETROL = "Car not working on Petrol";
        // Battery related
        String BATTERY_CC_NOT_AVAILABLE = "Not Available";
        //Engine Oil related
        String ENGINE_OIL_CC_LOW_PRESSURE_WARNING_LIGHT_GLOWING = "Low Pressure warning light glowing";
        //Coolant related
        String COLLANT_CC_RADIATOR_DAMAGED= "Radiator Damaged";
        String COLLANT_CC_MIXED_WITH_ENGINE_OIL= "Mixed with Engine Oil";
        // Exhaust Smoke related
        String EXHAUST_SMOKE_CC_LEAKAGE_FROM_MANIFOLD = "Leakage from manifold";
        String EXHAUST_SMOKE_CC_OIL_COMING_FROM_EXHAUST_TAIL_PIPE = "Oil coming from exhaust tail pipe";
    }
}
