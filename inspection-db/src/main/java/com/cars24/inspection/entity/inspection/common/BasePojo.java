package com.cars24.inspection.entity.inspection.common;

import lombok.Data;
@Data
public class BasePojo {
    public EstimatedRFC estimatedRFC;
    public Rating rating;
    public Comments comments;
    public Comments highlightPositives;
}

