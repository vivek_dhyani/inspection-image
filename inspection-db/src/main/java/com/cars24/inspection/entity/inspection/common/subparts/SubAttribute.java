package com.cars24.inspection.entity.inspection.common.subparts;

import com.cars24.inspection.entity.inspection.common.ParentAttribute;

public class SubAttribute extends ParentAttribute {

    public SubAttribute(){

    }

    public SubAttribute(String title, String value) {
        this.setTitle(title);
        this.setValue(value);
    }
}
