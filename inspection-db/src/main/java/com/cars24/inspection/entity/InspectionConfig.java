package com.cars24.inspection.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;

@Data
@Document("inspection_config")
public class InspectionConfig {

    @Id
    private ObjectId id;
    private String client;
    private String type;
    private StoreConfigs storeConfig;
}
