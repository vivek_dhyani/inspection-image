package com.cars24.inspection.entity.inspection.electricalinterior.subpart;

import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 16-11-2017.
 */

public class AirbagFeatureSubParts extends SubAttributes  {
    public SubAttribute driverSide;
    public SubAttribute coDriverSide;
    public SubAttribute lhsAPillar;
    public SubAttribute rhsAPillar;
    public SubAttribute lhsBPillar;
    public SubAttribute rhsBPillar;
    public SubAttribute rhsCPillar;
    public SubAttribute lhsCPillar;
}
