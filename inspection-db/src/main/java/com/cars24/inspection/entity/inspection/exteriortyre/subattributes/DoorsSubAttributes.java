package com.cars24.inspection.entity.inspection.exteriortyre.subattributes;


import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;

public class DoorsSubAttributes extends SubAttributes  {
    public SubAttribute lhsFront;
    public SubAttribute rhsFront;
    public SubAttribute lhsRear;
    public SubAttribute rhsRear;
//  public SubAttribute dicky;
}
