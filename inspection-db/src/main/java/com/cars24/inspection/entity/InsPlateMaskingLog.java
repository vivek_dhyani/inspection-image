package com.cars24.inspection.entity;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Data;
import java.util.ArrayList;

@Data
@Document("ins_plate_masking_log")
public class InsPlateMaskingLog {

    @Id
    private ObjectId id;
    private String appointmentId;
    private int storeId;
    private int flowType;
    private ArrayList<PlateMaskLog> request;
}
