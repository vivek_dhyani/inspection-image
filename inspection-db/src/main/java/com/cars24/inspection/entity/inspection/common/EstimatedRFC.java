package com.cars24.inspection.entity.inspection.common;

import lombok.Data;

@Data
public class EstimatedRFC {
    public String title;
    public long value;

    public EstimatedRFC(){}

    public EstimatedRFC(String title , long value){
        this.title = title;
        this.value = value;
    }
}
