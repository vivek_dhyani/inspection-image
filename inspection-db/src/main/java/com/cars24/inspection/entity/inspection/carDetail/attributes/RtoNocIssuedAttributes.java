package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RtoNocIssuedDateAttribute;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RtoNocIssuedForAttributes;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RtoNocIssuedSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.util.ReflectionUtil;


/**
 * @author Amrit Kumar on 21/2/19.
 */
public class RtoNocIssuedAttributes extends Attributes  {
    public RtoNocIssuedSubParts subParts;

    public void setRtoNocData(ImageCreateRequestDto imgDto){
        this.setTitle(imgDto.getPartTitle());
        this.setValue(imgDto.getPartValue());

        ReflectionUtil refUtill = new ReflectionUtil();

        if (this.subParts == null) {
            this.subParts = new RtoNocIssuedSubParts();
        }
        Object subPart = refUtill.getObjAttribute(this.subParts, imgDto.getSubPart(), true);

        if( subPart instanceof RtoNocAvailableAttributes){
            ((RtoNocAvailableAttributes)subPart).setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),imgDto.getId(), imgDto.getAliasValue());
        } else if(subPart instanceof RtoNocIssuedForAttributes){
            ((RtoNocIssuedForAttributes)subPart).setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),imgDto.getId(), imgDto.getAliasValue());
        } else if(subPart instanceof RtoNocIssuedDateAttribute){
            ((RtoNocIssuedDateAttribute)subPart).setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),imgDto.getId(), imgDto.getAliasValue());
        } else if(subPart instanceof RtoNmcAvailableAttributes){
            ((RtoNmcAvailableAttributes)subPart).setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),imgDto.getId(), imgDto.getAliasValue());
        }
    }
}
