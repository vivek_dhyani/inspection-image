package com.cars24.inspection.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Data
@Document("oauth_access_tokens")
public class OauthAccessTokens {

    @Id
    private Object id;
    private String access_token;
    private String client_id;
    private Long expires;
    private String user_id;
    private Integer location_id;
    private String role;
    private String email;

    public Long getExpires() {
        return expires;
    }

    public String getAccess_token() {
        return access_token;
    }
}
