package com.cars24.inspection.entity.inspection;

import com.cars24.inspection.constants.ConstantFields;
import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import com.cars24.inspection.entity.inspection.enginetransmission.EngineTransmission;
import com.cars24.inspection.entity.inspection.ac.AirConditioning;
import com.cars24.inspection.entity.inspection.carDetail.CarDetails;
import com.cars24.inspection.entity.inspection.electricalinterior.ElectricalInterior;
import com.cars24.inspection.entity.inspection.exteriortyre.ExteriorTyres;
import com.cars24.inspection.entity.inspection.ssb.SteeringSuspensionBrakes;
import com.cars24.inspection.entity.inspection.summary.Summary;

import com.cars24.inspection.util.ReflectionUtil;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class InspectionData {
    public EngineTransmission engineTransmission;
    public AirConditioning airConditioning;
    public ExteriorTyres exteriorTyres;
    public CarDetails carDetails;
    public SteeringSuspensionBrakes steeringSuspensionBrakes;
    public ElectricalInterior electricalsInterior;
    public Summary summary;

    public InspectionData() {
        engineTransmission = new EngineTransmission();
        electricalsInterior = new ElectricalInterior();
        airConditioning = new AirConditioning();
        exteriorTyres = new ExteriorTyres();
        carDetails = new CarDetails();
        steeringSuspensionBrakes = new SteeringSuspensionBrakes();
        summary = new Summary();
    }

    public Object setImageData(ImageCreateRequestDto imageDto)  {

        ReflectionUtil refUtil = new ReflectionUtil();
        Object section = refUtil.getObjAttribute(this, imageDto.getSection(), true);
        Object attr = new Object();
        if (imageDto.getType().equals(ConstantFields.AdditionalInfo)) {
            if (section instanceof EngineTransmission){
                 attr = ((EngineTransmission) section).setImageData(imageDto);
            } else if(section instanceof ElectricalInterior){
                attr = ((ElectricalInterior) section).setImageData(imageDto);
            } else if(section instanceof AirConditioning){
                attr = ((AirConditioning) section).setImageData(imageDto);
            } else if(section instanceof ExteriorTyres){
                attr = ((ExteriorTyres) section).setImageData(imageDto);
            } else if(section instanceof CarDetails){
                attr = ((CarDetails) section).setImageData(imageDto);
            } else if(section instanceof SteeringSuspensionBrakes){
                attr = ((SteeringSuspensionBrakes) section).setImageData(imageDto);
            }
        } else {
            attr = this.setMainImgData(section, refUtil, imageDto);

        }
        return attr;
    }

    private Object setMainImgData(Object section, ReflectionUtil refUtil, ImageCreateRequestDto imageDto){
        ArrayList <ImageWebModel>  imgwebModelList  = (ArrayList) refUtil.getObjAttribute(section, imageDto.getType(), true);
        if (!imgwebModelList.isEmpty()){
            Integer index  = -1;
            for (int i = 0; i < imgwebModelList.size(); i++) {
                ImageWebModel obj = imgwebModelList.get(i);
                if(obj.getHashKey() != null && obj.getHashKey().equals(imageDto.getHashKey())){
                    index = i;
                    break;
                }
            }
            ImageWebModel  newForm = new ImageWebModel(imageDto);
            if (index.equals(-1)){
                imgwebModelList.add(newForm);
            } else {
                imgwebModelList.get(index).createAndupdate(imageDto);
            }
        } else{
            ImageWebModel  newForm = new ImageWebModel(imageDto);
            imgwebModelList.add(newForm);
        }
        return imgwebModelList;
    }
}
