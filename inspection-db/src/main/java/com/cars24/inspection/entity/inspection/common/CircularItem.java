package com.cars24.inspection.entity.inspection.common;

import  lombok.Data;

@Data
public class CircularItem {
    public String value;
    public boolean isSelected = false;
}
