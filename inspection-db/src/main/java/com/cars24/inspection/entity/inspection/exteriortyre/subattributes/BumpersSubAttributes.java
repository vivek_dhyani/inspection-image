package com.cars24.inspection.entity.inspection.exteriortyre.subattributes;


import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

@Data
public class BumpersSubAttributes extends SubAttributes  {
    public SubAttribute rear;
    public SubAttribute front;
}
