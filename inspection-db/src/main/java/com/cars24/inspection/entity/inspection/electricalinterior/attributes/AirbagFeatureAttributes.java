package com.cars24.inspection.entity.inspection.electricalinterior.attributes;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.electricalinterior.subpart.AirbagFeatureSubParts;
import com.cars24.inspection.entity.inspection.electricalinterior.subpart.PowerWindowsSubParts;
import com.cars24.inspection.util.ReflectionUtil;

public class AirbagFeatureAttributes extends Attributes  {
    public AirbagFeatureSubParts subParts;

    public void setAirbagAtrributesData(ImageCreateRequestDto imgDto){
        this.setTitle(imgDto.getPartTitle());
        this.setValue(imgDto.getPartValue());
        if(imgDto.getAliasValue() !=null){
            this.setAliasValue(imgDto.getAliasValue());
        }
        if(this.subParts == null){
            this.subParts = new AirbagFeatureSubParts();
        }
        ReflectionUtil refUtill = new ReflectionUtil();
        SubAttribute subPart = (SubAttribute)refUtill.getObjAttribute(this.subParts, imgDto.getSubPart(), true);
        subPart.setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),null, imgDto.getAliasValue());
    }
}
