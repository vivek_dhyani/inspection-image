package com.cars24.inspection.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;

@Setter
@Getter
@Data
@Document("ins_positive_highlights")
public class PositiveHighlights {

    @Id
    private Object _id;
    private Integer id;
    private Integer rank;
    private String comment;
    private String status;
    private String section;
    private ArrayList<PositiveHighlightsFields> fields;
    private Integer[] removeComments;


    public Integer getId() {
        return this.id;
    }

    public Integer getRank() {
        return this.rank;
    }

    public String getComment() {
        return this.comment;
    }


    public ArrayList<PositiveHighlightsFields> getFields() {

        return this.fields;
    }

    public Integer[] getRemoveComments() {

        return removeComments;
    }
}
