package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.FitnessUptoSupbarts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;

public class FitnessUptoAttributes extends Attributes {
    public FitnessUptoSupbarts subParts;
}
