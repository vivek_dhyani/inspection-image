package com.cars24.inspection.entity.inspection.exteriortyre.subattributes;

import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;

public class PillarsSubAttributes extends SubAttributes  {
    public SubAttribute lhsA;
    public SubAttribute rhsA;
    public SubAttribute lhsB;
    public SubAttribute rhsB;
    public SubAttribute lhsC;
    public SubAttribute rhsC;
}
