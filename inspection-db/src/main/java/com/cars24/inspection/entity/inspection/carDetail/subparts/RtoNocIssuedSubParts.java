package com.cars24.inspection.entity.inspection.carDetail.subparts;

import com.cars24.inspection.entity.inspection.carDetail.attributes.RtoNmcAvailableAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.RtoNocAvailableAttributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;
import lombok.Data;

@Data
public class RtoNocIssuedSubParts extends SubAttributes  {
    public RtoNocAvailableAttributes rtoNocAvailable;
    public RtoNocIssuedForAttributes rtoNocIssuedFor;
    public RtoNocIssuedDateAttribute rtoNocIssuedDate;
    public RtoNmcAvailableAttributes rtoNmcAvailable;
}
