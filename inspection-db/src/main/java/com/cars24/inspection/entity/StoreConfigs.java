package com.cars24.inspection.entity;
import  lombok.Data;

@Data
public class StoreConfigs {
    private StoreConfig numberPlateMask;
}
