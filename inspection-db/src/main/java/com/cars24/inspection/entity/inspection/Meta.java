package com.cars24.inspection.entity.inspection;

import com.cars24.inspection.entity.Order;
import lombok.Data;

@Data
public class Meta {
    private Order order;
}
