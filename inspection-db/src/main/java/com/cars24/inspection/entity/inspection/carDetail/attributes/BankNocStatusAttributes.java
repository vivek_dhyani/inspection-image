package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.BankNocStatusSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;

public class BankNocStatusAttributes extends Attributes {
    public BankNocStatusSubParts subParts;
}
