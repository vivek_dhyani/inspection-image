package com.cars24.inspection.entity.inspection.exteriortyre.subattributes;


import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;

public class WindshieldSubAttributes extends SubAttributes  {
    public SubAttribute front;
    public SubAttribute rear;
}
