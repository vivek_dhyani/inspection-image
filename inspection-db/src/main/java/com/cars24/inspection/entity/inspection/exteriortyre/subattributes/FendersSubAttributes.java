package com.cars24.inspection.entity.inspection.exteriortyre.subattributes;


import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;
import lombok.Data;

@Data
public class FendersSubAttributes extends SubAttributes  {
    public SubAttribute lhs;
    public SubAttribute rhs;
}
