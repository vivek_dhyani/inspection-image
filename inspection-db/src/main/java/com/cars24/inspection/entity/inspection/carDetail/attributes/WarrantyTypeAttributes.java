package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.WarrantyTypeSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class WarrantyTypeAttributes extends Attributes  {
    public WarrantyTypeSubParts subParts;
}
