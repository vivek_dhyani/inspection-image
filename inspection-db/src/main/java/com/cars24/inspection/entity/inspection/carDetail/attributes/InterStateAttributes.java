package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.InterStateSubparts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



/**
 * @author Saunik on 17-01-2018.
 */

public class InterStateAttributes extends Attributes  {
    public InterStateSubparts subParts;
}
