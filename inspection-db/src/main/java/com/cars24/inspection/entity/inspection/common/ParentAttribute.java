package com.cars24.inspection.entity.inspection.common;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.additionalinfo.AdditionalInfo;
import java.util.ArrayList;
import lombok.Data;

import javax.validation.constraints.Null;

@Data
public class ParentAttribute{
    public String title;
    public String value;
    public String id;
    public ArrayList<AdditionalInfo> additionalInfo;
    public ImageWebModel image;
    public String aliasValue;

    public void setAtrributesData(ImageCreateRequestDto imgDto, String title , String value, @Null String id, @Null String alias){
        this.setTitle(title);
        this.setValue(value);

        if(id != null){
            this.setId(id);
        }
        if(alias !=null){
            this.setAliasValue(alias);
        }

        ArrayList<AdditionalInfo> additionalInfo = new ArrayList<AdditionalInfo>();
        additionalInfo.add(new AdditionalInfo(imgDto));
        this.setAdditionalInfo(additionalInfo);
    }
}
