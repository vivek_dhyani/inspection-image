package com.cars24.inspection.entity.inspection.summary;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.BasePojo;
import com.cars24.inspection.entity.inspection.common.Rating;



/**
 * @author Saunik on 16-11-2017.
 */

public class Summary extends BasePojo  {
    public Rating exteriorTyres;
    public Rating electricalsInterior;
    public Rating engineTransmission;
    public Rating steeringSuspensionBrakes;
    public Rating airConditioning;
}
