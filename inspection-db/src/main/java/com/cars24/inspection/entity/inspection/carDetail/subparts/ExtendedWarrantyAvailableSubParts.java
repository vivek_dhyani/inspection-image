package com.cars24.inspection.entity.inspection.carDetail.subparts;

import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;

public class ExtendedWarrantyAvailableSubParts  extends SubAttributes{
    public SubAttribute extendedWarrantyExpiryDate;
}