package com.cars24.inspection.entity.inspection.electricalinterior;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.BasePojo;
import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.electricalinterior.attributes.AirbagFeatureAttributes;
import com.cars24.inspection.entity.inspection.electricalinterior.attributes.PowerWindowsAttributes;
import java.util.ArrayList;

import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

@Data
public class ElectricalInterior extends BasePojo  {
    public Attributes powerWindowsNumber;
    public PowerWindowsAttributes powerWindows;
    public Attributes electrical;
    public Attributes interior;
    public Attributes numberOfAirbags;
    public AirbagFeatureAttributes airbagFeature;
    public Attributes musicSystem;
    public Attributes leatherSeats;
    public Attributes fabricSeats;
    public Attributes sunroof;
    public Attributes steeringMountedAudioControls;
    public Attributes abs;
    public Attributes rearDefogger;
    public Attributes reverseCamera;
    public Attributes odometerReading;
    public ArrayList<ImageWebModel> mainImages;
    public ArrayList<ImageWebModel> mainVideos;

    public Object setImageData(ImageCreateRequestDto imgDto){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object part =  refUtil.getObjAttribute(this, imgDto.getPart(), true);
        if (part instanceof PowerWindowsAttributes){
            ((PowerWindowsAttributes) part).setPowerWindowAtrributesData(imgDto);

        } else if (part instanceof AirbagFeatureAttributes){
            ((AirbagFeatureAttributes) part).setAirbagAtrributesData(imgDto);

        }else if (part instanceof Attributes){
             ((Attributes) part).setAtrributesData(imgDto, imgDto.getPartTitle(), imgDto.getPartValue(), imgDto.getId(), imgDto.getAliasValue());
        }
        return part;
    }
}
