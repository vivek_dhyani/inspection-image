package com.cars24.inspection.entity.inspection.electricalinterior.subpart;

import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 16-11-2017.
 */

public class PowerWindowsSubParts extends SubAttributes  {
    public SubAttribute lhsFront;
    public SubAttribute rhsFront;
    public SubAttribute lhsRear;
    public SubAttribute rhsRear;
}
