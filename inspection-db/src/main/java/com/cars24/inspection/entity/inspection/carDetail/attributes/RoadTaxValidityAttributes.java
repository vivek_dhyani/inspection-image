package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.RoadTaxValiditySubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class RoadTaxValidityAttributes extends Attributes  {
    public RoadTaxValiditySubParts subParts;
}
