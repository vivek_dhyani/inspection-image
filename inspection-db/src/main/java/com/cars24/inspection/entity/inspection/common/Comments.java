package com.cars24.inspection.entity.inspection.common;

import java.util.ArrayList;
import java.util.Iterator;

import lombok.Data;

@Data
public class Comments {

    public String title;
    public ArrayList<Comment> value;
    public Comments(){}
    public Comments(String title) {
        this.title = title;
        this.value = new ArrayList<>();
    }
}