package com.cars24.inspection.entity.inspection.common;

import lombok.Data;
@Data
public class Rating{
    public float value;
    public String title;
}

