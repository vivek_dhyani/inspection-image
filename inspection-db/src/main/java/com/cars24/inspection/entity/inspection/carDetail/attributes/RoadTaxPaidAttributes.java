package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.RoadTaxPaidSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



public class RoadTaxPaidAttributes extends Attributes  {
    public RoadTaxPaidSubParts subParts;
}
