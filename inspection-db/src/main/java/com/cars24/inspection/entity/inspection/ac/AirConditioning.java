package com.cars24.inspection.entity.inspection.ac;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.BasePojo;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

@Data
public class AirConditioning extends BasePojo {
    public Attributes acCooling;
    public Attributes heater;
    public Attributes climateControlAc;

    public Object setImageData(ImageCreateRequestDto imgDto){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object part =  refUtil.getObjAttribute(this, imgDto.getPart(), true);
        if (part instanceof Attributes){
            ((Attributes) part).setAtrributesData(imgDto, imgDto.getPartTitle(), imgDto.getPartValue(), imgDto.getId(), imgDto.getAliasValue());
        }
        return part;
    }
}
