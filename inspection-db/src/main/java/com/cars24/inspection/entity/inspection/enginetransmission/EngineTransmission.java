package com.cars24.inspection.entity.inspection.enginetransmission;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;
import com.cars24.inspection.entity.inspection.common.BasePojo;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;

@Data
public class EngineTransmission extends BasePojo {
    public Attributes battery;
    public Attributes engineOilLevelDipstik;
    public Attributes engineOil;
    public Attributes coolant;
    public Attributes engineMounting;
    public Attributes engineSound;
    public Attributes exhaustSmoke;
    public Attributes engineBlowByBackCompression;
    public Attributes clutch;
    public Attributes gearShifting;
    public Attributes engine;



    public Object setImageData(ImageCreateRequestDto imgDto){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object part =  refUtil.getObjAttribute(this, imgDto.getPart(), true);
        if (part instanceof Attributes){
            ((Attributes) part).setAtrributesData(imgDto, imgDto.getPartTitle(), imgDto.getPartValue(), imgDto.getId(), imgDto.getAliasValue());
        }
        return part;
    }
}
