package com.cars24.inspection.entity.inspection.carDetail.subparts;

import com.cars24.inspection.entity.inspection.carDetail.attributes.TPermitTypeAttributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class UsedVehicleAsSubParts extends SubAttributes  {
    public SubAttribute fitnessExpiryDate;
    public TPermitTypeAttributes tPermitType;
}
