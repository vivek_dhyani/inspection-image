package com.cars24.inspection.entity.inspection.common;

import lombok.Data;

@Data
public class Comment {
    public String comment;
    public int id;
    public int rank;
    public String type;
}
