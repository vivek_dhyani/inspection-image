package com.cars24.inspection.entity;

import com.cars24.inspection.util.DateUtil;
import lombok.Data;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Data
public class PlateMaskLog {
    private String id;
    private LocalDateTime createdAt;
    private String appointmentId;
    private int storeId;
    private String category;
    private String key;
    private String url;

    public PlateMaskLog(){

    }

    public PlateMaskLog(Map<String, Object> logData){
        this.id = (String) logData.get("id");
        this.appointmentId = (String) logData.get("appointmentId");
        this.storeId = (int) logData.get("storeId");
        this.category = (String) logData.get("category");
        this.key = (String) logData.get("key");
        this.url = (String) logData.get("url");
        this.createdAt = DateUtil.getCurrentTime();
    }
}
