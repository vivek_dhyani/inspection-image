package com.cars24.inspection.entity.inspection.ssb;

import com.cars24.inspection.entity.inspection.common.*;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

import java.util.ArrayList;

@Data
public class SteeringSuspensionBrakes {
    public Attributes steering;
    public Attributes suspension;
    public Attributes brakes;
    public EstimatedRFC estimatedRFC;
    public Rating rating;
    public RejectionRemarks rejectionRemarks;
    public Comments comments;
    public Comments highlightPositives;

    public Object setImageData(ImageCreateRequestDto imgDto){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object part =  refUtil.getObjAttribute(this, imgDto.getPart(), true);
        if (part instanceof Attributes){
            ((Attributes) part).setAtrributesData(imgDto, imgDto.getPartTitle(), imgDto.getPartValue(), imgDto.getId(), imgDto.getAliasValue());
        }
        return part;
    }

    public void setDefaultEstimatedRFC() {
        this.estimatedRFC = new EstimatedRFC("ESTIMATED RFC", 0);
    }

    public void setDefaultComments(){
        this.comments = new Comments("Comments");
    }

    public void addRejectionRemark(ArrayList<RejectionRemark> rejRemark){
        this.rejectionRemarks = new RejectionRemarks(rejRemark);
    }

    public void setDefaultHighlightPositives(){
        this.highlightPositives = new Comments("Highlight Positives");
    }
}