package com.cars24.inspection.entity.inspection.carDetail;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.carDetail.attributes.LocationData;
import com.cars24.inspection.entity.inspection.carDetail.attributes.ChassisNumberAttribute;
import com.cars24.inspection.entity.inspection.carDetail.attributes.FuelTypeAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.InsuranceTypeAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.InterStateAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.IsUnderHypothecationAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.RADetailsAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.RcAvailabilityAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.RegistrationStateAttribute;
import com.cars24.inspection.entity.inspection.carDetail.attributes.RoadTaxPaidAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.RtoNocIssuedAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.StoreNameAttribute;
import com.cars24.inspection.entity.inspection.carDetail.attributes.VariantAttributes;
import com.cars24.inspection.entity.inspection.carDetail.attributes.ExtendedWarrantyAvailableAttributes;
import com.cars24.inspection.entity.inspection.common.BasePojo;
import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

import java.util.ArrayList;
@Data
public class CarDetails extends BasePojo {
    public Attributes appointmentId;
    public Attributes city;
    public Attributes inspectionAt;
    public StoreNameAttribute storeName;
    public Attributes make;
    public Attributes model;
    public Attributes year;
    public Attributes month;
    public VariantAttributes variant;
    public Attributes registrationYear;
    public Attributes registrationMonth;
    public Attributes fitnessUpto;
    public RegistrationStateAttribute registrationState;
    public Attributes registrationCity;
    public Attributes registrationNumber;
    public Attributes ownerNumber;
    public Attributes toBeScrapped;
    public Attributes duplicateKey;
    public FuelTypeAttributes fuelType;
    public Attributes cngLpgFitment;
    public InsuranceTypeAttributes insuranceType;
    public Attributes customerName;
    public ArrayList<ImageWebModel> chassisNumberImage;
    public ChassisNumberAttribute chassisNumber;
    public Attributes chassisNumberEmbossing;
    public RcAvailabilityAttributes rcAvailability;
    public RoadTaxPaidAttributes roadTaxPaid;
    public InterStateAttributes interState;
    public RtoNocIssuedAttributes rtoNocIssued;
    public IsUnderHypothecationAttributes isUnderHypothecation;
    public RADetailsAttributes raDetails;
    public ExtendedWarrantyAvailableAttributes extendedWarranty;
    public ArrayList<ImageWebModel> warrantyImages;
    public ArrayList<ImageWebModel> rcImages;
    public ArrayList<ImageWebModel> insuranceImages;
    public ArrayList<ImageWebModel> additionalImages;
    public ArrayList<ImageWebModel> hypothecationImages;
    public Attributes insuranceNudge;
    public LocationData geoLocation;
    public Attributes distanceTraveled;
    public Attributes partiPeshi;

    public Object setImageData(ImageCreateRequestDto imgDto){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object part =  refUtil.getObjAttribute(this, imgDto.getPart(), true);
        if (part instanceof RtoNocIssuedAttributes){
            ((RtoNocIssuedAttributes) part).setRtoNocData(imgDto);

        } else if (part instanceof RcAvailabilityAttributes){
            ((RcAvailabilityAttributes) part).setRCAvailabilityData(imgDto);

        } else if (part instanceof Attributes){
            ((Attributes) part).setAtrributesData(imgDto, imgDto.getPartTitle(), imgDto.getPartValue(), imgDto.getId(), imgDto.getAliasValue());

        }
        return part;
    }
}
