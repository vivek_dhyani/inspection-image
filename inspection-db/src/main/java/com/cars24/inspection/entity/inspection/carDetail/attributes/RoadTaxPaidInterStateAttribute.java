package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.RoadTaxPaidInterStateSubPart;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



/**
 * @author Amrit.Kumar on 12/26/2017.
 */

public class RoadTaxPaidInterStateAttribute extends Attributes  {
    public RoadTaxPaidInterStateSubPart subParts;
}
