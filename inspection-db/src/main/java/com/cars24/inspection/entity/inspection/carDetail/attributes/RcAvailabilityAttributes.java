package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RcAvailabilitySubParts;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RcConditionSubParts;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RtoNocIssuedDateAttribute;
import com.cars24.inspection.entity.inspection.carDetail.subparts.RtoNocIssuedForAttributes;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

@Data
public class RcAvailabilityAttributes extends Attributes  {
    public RcAvailabilitySubParts subParts;

    public void setRCAvailabilityData(ImageCreateRequestDto imgDto){
        this.setTitle(imgDto.getPartTitle());
        this.setValue(imgDto.getPartValue());

        if(this.subParts == null){
            this.subParts = new RcAvailabilitySubParts();
        }
        ReflectionUtil refUtill = new ReflectionUtil();
        Object subPart = refUtill.getObjAttribute(this.subParts, imgDto.getSubPart(), true);

        if( subPart instanceof SubAttribute){
            ((SubAttribute)subPart).setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),imgDto.getId(), imgDto.getAliasValue());
        } else if(subPart instanceof RcConditionAttributes){
            RcConditionAttributes rcConditonSubpart = ((RcConditionAttributes)subPart);
            rcConditonSubpart.setTitle(imgDto.getSubPartTitle());
            rcConditonSubpart.setTitle(imgDto.getSubPartValue());

            if (rcConditonSubpart.subParts == null) {
                rcConditonSubpart.subParts = new RcConditionSubParts();
            }
            SubAttribute subSubPart = (SubAttribute)refUtill.getObjAttribute(rcConditonSubpart.subParts, imgDto.getSubSubPart(), true);
            subSubPart.setAtrributesData(imgDto, imgDto.getSubSubPartTitle(), imgDto.getSubSubPartValue(), imgDto.getId(), imgDto.getAliasValue());
        }
    }
}
