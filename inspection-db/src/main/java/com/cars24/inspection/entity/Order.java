package com.cars24.inspection.entity;

import lombok.Data;

@Data
public class Order {
    private String orderState;
    private String canReInspect;
}
