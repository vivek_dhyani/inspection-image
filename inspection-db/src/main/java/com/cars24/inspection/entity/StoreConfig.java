package com.cars24.inspection.entity;

import java.util.ArrayList;
import lombok.Data;

@Data
public class StoreConfig {
    private Boolean applyAllStores;
    private ArrayList<Integer> applyAtStores;
}
