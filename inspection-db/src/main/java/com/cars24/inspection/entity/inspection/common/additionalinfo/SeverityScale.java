package com.cars24.inspection.entity.inspection.common.additionalinfo;

import  lombok.Data;

/**
 * @author vivek.dhyani@cars24.com.
 */
@Data
public class SeverityScale {
    public String Scratched;
    public String Dented;
    public String Damaged;
    public String Rusted;
}
