package com.cars24.inspection.entity.inspection;


import com.cars24.inspection.entity.inspection.common.AppInfo;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Data
@Document("inspection")
public class Inspection {

    @Id
    private ObjectId id;
    private String appointmentId;
    private String inspectionStatus;
    private Integer carId;
    private InspectionData inspectionData;
    private Integer version;
    private Integer storeId;
    private String carCode;
    private String resourcePath;
    private String isCritical;
    private String inspectionBy;
    private LocalDateTime inspectionStartTime;
    private LocalDateTime inspectionEndTime;
    private LocalDateTime endTime;
    private Meta meta;
    public ArrayList<AppInfo> appDetails;

    public void setResourcePath() {
        LocalDate currentdate = LocalDate.now();
        this.resourcePath = currentdate.getYear()+"/"+currentdate.getMonthValue()+"/"+currentdate.getDayOfMonth()+"/";
    }

    public String getResourcePath() {
        return resourcePath;
    }
}