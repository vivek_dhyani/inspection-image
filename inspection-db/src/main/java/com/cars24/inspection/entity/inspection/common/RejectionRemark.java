package com.cars24.inspection.entity.inspection.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.cars24.inspection.util.DateUtil;
import  lombok.Data;

@Data
public class RejectionRemark {
    public String role;
    public String user;
    public String rejectionRemark;
    public String timestamp;

    public RejectionRemark(){}

    public RejectionRemark(String role, String user, String rejectionRemark){
        this.role = role;
        this.user = user;
        this.rejectionRemark = rejectionRemark;
        DateTimeFormatter formater = DateTimeFormatter.ISO_DATE_TIME;
        this.timestamp = DateUtil.getCurrentTime().format(formater);
    }
}
