package com.cars24.inspection.entity.inspection.carDetail.subparts;

import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 04-12-2017.
 */

public class StoreNameSubParts extends SubAttributes  {
    public BlockedDealers blockedDealers;
    public DealerListingPrice dealerListingPrice;
}
