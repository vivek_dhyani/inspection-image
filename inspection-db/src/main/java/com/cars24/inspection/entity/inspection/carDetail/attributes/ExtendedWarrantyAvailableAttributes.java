package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.ExtendedWarrantyAvailableSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



/**
 * @author  Amrit.Chakradhari 2020-07-09
 */
public class ExtendedWarrantyAvailableAttributes extends Attributes  {
    public ExtendedWarrantyAvailableSubParts subParts;
}
