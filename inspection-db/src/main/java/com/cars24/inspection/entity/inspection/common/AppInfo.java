package com.cars24.inspection.entity.inspection.common;
import lombok.Data;

@Data
public class AppInfo
{
    private String role;
    private String version;
    private String inspectionStatus;
    private String deviceName;
    private String osVer;
    public String uniqueID;
    public AppInfo()
    {
        role="";
        version="";
        inspectionStatus="";
        deviceName="";
        osVer="";
        uniqueID = "";
    }
}