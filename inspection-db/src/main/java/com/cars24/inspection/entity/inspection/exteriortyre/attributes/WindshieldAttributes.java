package com.cars24.inspection.entity.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.exteriortyre.subattributes.ApronSubAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.subattributes.WindshieldSubAttributes;
import com.cars24.inspection.util.ReflectionUtil;

public class WindshieldAttributes extends Attributes  {
    public WindshieldSubAttributes subParts;

    public void setWindShieldAttributesData(ImageCreateRequestDto imgDto){
        this.setTitle(imgDto.getPartTitle());
        this.setValue(imgDto.getPartValue());
        if(imgDto.getAliasValue() !=null){
            this.setAliasValue(imgDto.getAliasValue());
        }
        if (this.subParts == null) {
            this.subParts = new WindshieldSubAttributes();
        }
        ReflectionUtil refUtill = new ReflectionUtil();
        SubAttribute subPart = (SubAttribute)refUtill.getObjAttribute(this.subParts, imgDto.getSubPart(), true);
        subPart.setAtrributesData(imgDto, imgDto.getSubPartTitle(), imgDto.getSubPartValue(),null, imgDto.getAliasValue());
    }
}
