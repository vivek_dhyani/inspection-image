package com.cars24.inspection.entity.inspection.carDetail.subparts;

import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;

public class BlockedDealers extends SubAttribute {
    public int dealerId;
    public DealersSubParts subParts;
}
