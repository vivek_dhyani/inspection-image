package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.entity.inspection.carDetail.subparts.IsUnderHypothecationSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;


/**
 * @author Saunik on 14-11-2017.
 */

public class IsUnderHypothecationAttributes extends Attributes  {
    public IsUnderHypothecationSubParts subParts;
}
