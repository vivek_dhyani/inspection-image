package com.cars24.inspection.entity.inspection.carDetail.subparts;

import com.cars24.inspection.entity.inspection.common.attributes.Attributes;



/**
 * @author Praveen.Sharma on 8/14/2018.
 */
public class InterStateNocIssuedForAttribute extends Attributes  {
    public NocValiditySubPart subParts;
}
