package com.cars24.inspection.entity.inspection.carDetail.attributes;

public class LocationData{

    public double lat;
    public double lng;

    public LocationData() {
    }

    public LocationData(double latitude, double longitude) {
        this.lat = latitude;
        this.lng = longitude;
    }
}