package com.cars24.inspection.entity.inspection.exteriortyre;

import com.cars24.inspection.constants.ConstantFields;
import com.cars24.inspection.entity.inspection.common.Comments;
import com.cars24.inspection.entity.inspection.common.EstimatedRFC;
import com.cars24.inspection.entity.inspection.common.Rating;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.common.BasePojo;
import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import com.cars24.inspection.entity.inspection.common.RejectionRemark;
import com.cars24.inspection.entity.inspection.common.RejectionRemarks;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.enginetransmission.EngineTransmission;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.ApronAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.BumpersAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.DoorsAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.FendersAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.LightsAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.ORVMAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.PillarsAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.QuarterPanelAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.RunningBorderAttributes;
import com.cars24.inspection.entity.inspection.exteriortyre.attributes.WindshieldAttributes;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

import java.util.ArrayList;

@Data
public class ExteriorTyres {
    public BumpersAttributes bumpers;
    public Attributes bonnetHood;
    public Attributes roof;
    public FendersAttributes fenders;
    public DoorsAttributes doors;
    public PillarsAttributes pillars;
    public RunningBorderAttributes runningBorder;
    public QuarterPanelAttributes quarterPanel;
    public Attributes dickyDoorBootDoor;
    public Attributes bootFloor;
    public ApronAttributes apron;
    public Attributes firewall;
    public Attributes cowlTop;
    public Attributes lowerCrossMember;
    public Attributes bonnetPatti;
    public Attributes headLightSupport;
    public Attributes radiatorSupport;
    public Attributes frontShow;
    public WindshieldAttributes windshield;
    public ORVMAttributes orvm;
    public LightsAttributes lights;
    public LightsAttributes fogLight;
    public Attributes alloyWheels;
    public Attributes lhsFrontTyre;
    public Attributes rhsFrontTyre;
    public Attributes lhsRearTyre;
    public Attributes rhsRearTyre;
    public Attributes spareTyre;
    public ArrayList<ImageWebModel> mainImages;
    public ArrayList<ImageWebModel> mainVideos;
    public EstimatedRFC estimatedRFC;
    public Rating rating;
    public Comments comments;
    public Comments highlightPositives;
    public RejectionRemarks rejectionRemarks;

    public Object setImageData(ImageCreateRequestDto imgDto){
        ReflectionUtil refUtil = new ReflectionUtil();
        Object part =  refUtil.getObjAttribute(this, imgDto.getPart(), true);

        if (part instanceof BumpersAttributes){
           ((BumpersAttributes) part).setBumpersPart(imgDto);

        } else if (part instanceof FendersAttributes){
            ((FendersAttributes) part).setFendersSubPart(imgDto);

        } else if (part instanceof DoorsAttributes){
           ((DoorsAttributes) part).setDoorsAttributesData(imgDto);

        } else if (part instanceof PillarsAttributes){
           ((PillarsAttributes) part).setPillarsAttributesData(imgDto);

        } else if (part instanceof RunningBorderAttributes){
            ((RunningBorderAttributes) part).setRunningBorderAttributesData(imgDto);

        } else if (part instanceof QuarterPanelAttributes){
           ((QuarterPanelAttributes) part).setQurarterPanelAttributesData(imgDto);

        } else if (part instanceof ApronAttributes){
           ((ApronAttributes) part).setApronAttributesData(imgDto);

        } else if (part instanceof WindshieldAttributes){
           ((WindshieldAttributes) part).setWindShieldAttributesData(imgDto);

        } else if (part instanceof ORVMAttributes){
           ((ORVMAttributes) part).setOrvmAttributesData(imgDto);

        } else if (part instanceof LightsAttributes){
            ((LightsAttributes) part).setLightAttributesData(imgDto);

        }else if (part instanceof Attributes){
            ((Attributes) part).setAtrributesData(imgDto, imgDto.getPartTitle(), imgDto.getPartValue(), imgDto.getId(), imgDto.getAliasValue());
        }
        return part;
    }


    public void setDefaultEstimatedRFC() {
        this.estimatedRFC = new EstimatedRFC("ESTIMATED RFC", 0);
    }

    public void setDefaultComments(){
        this.comments = new Comments("Comments");
    }

    public void addRejectionRemark(ArrayList<RejectionRemark> rejRemark){
        this.rejectionRemarks = new RejectionRemarks(rejRemark);
    }

    public void setDefaultHighlightPositives(){
        this.highlightPositives = new Comments("Highlight Positives");
    }

}
