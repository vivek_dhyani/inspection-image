package com.cars24.inspection.entity.inspection.carDetail.attributes;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.carDetail.subparts.StoreNameSubParts;
import com.cars24.inspection.entity.inspection.common.attributes.Attributes;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

@Data
public class StoreNameAttribute extends Attributes  {
    public StoreNameSubParts subParts;
    public String regionId;
    public String regionName;
}
