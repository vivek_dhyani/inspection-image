package com.cars24.inspection.entity.inspection.common.additionalinfo;

import lombok.Data;
/**
 * @author vivek.dhyani@cars24.com.
 */
@Data
public class SeverityScaleCC {
    public String title;
    public SeverityScale value;
}
