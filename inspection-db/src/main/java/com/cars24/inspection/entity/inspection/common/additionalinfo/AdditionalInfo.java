package com.cars24.inspection.entity.inspection.common.additionalinfo;

import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import lombok.Data;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import java.util.List;
import java.util.Map;

@Data
public class AdditionalInfo{
    public AdditionalInfoSubAttribute currentCondition;
    public ImageWebModel image;
    public ImageWebModel video;
    public AdditionalInfoSubAttribute treadDepth;
    public AdditionalInfoSubAttribute workDone;
    public AdditionalInfoSubAttribute workToBeDone;
    public AdditionalInfoSubAttribute factorySealant;
    public AdditionalInfoSubAttribute panelPaint;
    public AdditionalInfoSubAttribute engineSoundScale;
    public String maskedBy;
    public String hashKey;
    public int syncState;
    public String part;
    public String subPart;
    public SeverityScaleCC severityScaleCC;

    public AdditionalInfo(){
    }

    public AdditionalInfo(ImageCreateRequestDto imgDto){
        this.hashKey = imgDto.getHashKey();
        if(imgDto.getCurrentCondition() != null) {
            Map<String, List<String>> ccMap =  imgDto.getCurrentCondition();
            this.currentCondition =  new  AdditionalInfoSubAttribute(ccMap.get("title").get(0), ccMap.get("value"));
        }

        if(imgDto.getWorkDone() != null) {
            Map<String, List<String>> wdMap =  imgDto.getWorkDone();
            this.workDone =  new  AdditionalInfoSubAttribute(wdMap.get("title").get(0), wdMap.get("value"));
        }

        if(imgDto.getTreadDepth() != null) {
            Map<String, List<String>> tdMap =  imgDto.getTreadDepth();
            this.treadDepth =  new  AdditionalInfoSubAttribute(tdMap.get("title").get(0), tdMap.get("value"));
        }

        if(imgDto.getImageFile() != null){
            this.image = new ImageWebModel(imgDto);
        }

        if(imgDto.getVideoFile() != null){
            this.video = new ImageWebModel(imgDto);
        }

    }
}
