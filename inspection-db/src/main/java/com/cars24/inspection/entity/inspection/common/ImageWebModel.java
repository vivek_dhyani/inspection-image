package com.cars24.inspection.entity.inspection.common;

import com.cars24.inspection.constants.IConstant;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.util.DateUtil;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.TimeZone;

@Data
public class ImageWebModel {
    public String title;
    public String hashKey;
    public String field;
    public String url;
    public String originalUrl;
    public String thumbUrl;
    public String type;
    public String carId;
    public int syncState; // refer to IConstants sync states.
    public String section;
    public String role;
    public String subPart;
    public String subSubPart;
    public String part;
    public String maskedBy;
    public String errorMessage;
    public String uploadTime;
    public String subSubPartTitle;
    public String subSubPartValue;
    public UtmSource Qc;
    public UtmSource Cjedit;
    public String partTitle;
    public String partValue;
    public int pointerCount;
    public int isTilted;
    public String fieldKey;
    public String utmSource;
    public ArrayList<DamageMarker> markers = null;
    public String subPartTitle;
    public String subPartValue;
    public LocalDateTime serverTime;
    //public LocalDateTime imageUploadTime;

    public ImageWebModel() {
        this.syncState = IConstant.IMAGE_SYNC_STATE_NOT_STARTED;
    }

    public ImageWebModel(ImageCreateRequestDto imgDto){
        this.createAndupdate(imgDto);
    }

    public void createAndupdate(ImageCreateRequestDto imgDto){
        this.title = imgDto.getTitle();
        this.url = imgDto.getImageUrl() != null ? imgDto.getImageUrl()  : imgDto.getVideoUrl();
        this.hashKey = imgDto.getHashKey();
        if (imgDto.getThumbUrl() != null){
            this.thumbUrl = imgDto.getThumbUrl();
        }
        if(imgDto.getField() != null){
            this.field = imgDto.getField();
        }
        this.isTilted = imgDto.getIsTilted() != null ? imgDto.getIsTilted() : 0;
        this.pointerCount = imgDto.getPointerCount() !=null ? imgDto.getPointerCount() : 0;
        this.serverTime = DateUtil.getCurrentTime();
        //this.imageUploadTime = imgDto.getUploadTime() != null ? (DateUtil.getDateFromTimeStamp(imgDto.getUploadTime())) : DateUtil.getCurrentTime();
    }

    public String getObjectHashCode(){
        return String.format("%1s-%2s-%3s",this.carId,this.hashKey,this.url);
    }
}
