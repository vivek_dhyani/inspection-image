package com.cars24.inspection.entity.inspection.exteriortyre.subattributes;


import com.cars24.inspection.entity.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.entity.inspection.common.subparts.SubAttributes;
import lombok.Data;

@Data
public class LightsSubAttributes extends SubAttributes  {
    public SubAttribute lhsHeadlight;
    public SubAttribute rhsHeadlight;
    public SubAttribute lhsTaillight;
    public SubAttribute rhsTaillight;
    public SubAttribute stopperLight;
    public SubAttribute lhsFogLight;
    public SubAttribute rhsFogLight;
}
