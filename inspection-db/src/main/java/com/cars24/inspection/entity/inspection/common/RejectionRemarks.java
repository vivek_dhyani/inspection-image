package com.cars24.inspection.entity.inspection.common;

import lombok.Data;

import java.util.ArrayList;

@Data
public class RejectionRemarks {
    public String title;
    public ArrayList<RejectionRemark> value;

    public RejectionRemarks(){}

    public RejectionRemarks(ArrayList<RejectionRemark> value) {
        this.title = "Rejection Remark";
        this.value = value;
    }
}
