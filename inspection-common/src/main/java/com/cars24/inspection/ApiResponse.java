package com.cars24.inspection;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import javax.validation.constraints.Null;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ApiResponse <T> {

    private Integer status;
    private String title;
    private String type;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long timestamp;
    private T detail;


    public static <T> ResponseEntity<ApiResponse<T>> buildWithSuccess(
            Integer status, T data, String title, String type, @Null Long timestamp
    ) {
        // Constructing response with success status and details
        ApiResponse<T> responseEntity = new ApiResponse<>();
        responseEntity.setStatus(status);
        responseEntity.setData(data);
        responseEntity.setTitle(title);
        responseEntity.setType(type);
        if(timestamp != null){
            responseEntity.setTimestamp(timestamp);
        }
        return ResponseEntity.status(HttpStatus.OK).body(responseEntity);
    }

    public static <Object> ResponseEntity<ApiResponse<Object>> buildWithError(
            Integer status, Object data, String title, String type, @Null Long timestamp
    ) {
        // Constructing response with success status and details
        ApiResponse<Object> responseEntity = new ApiResponse<>();
        responseEntity.setStatus(status);
        responseEntity.setData(data);
        responseEntity.setTitle(title);
        responseEntity.setType(type);
        if(timestamp != null){
            responseEntity.setTimestamp(timestamp);
        }
        return ResponseEntity.status(HttpStatus.valueOf(status)).body(responseEntity);
    }

    public void setStatus(Integer statusCode) {
        this.status = statusCode;
    }

    public void setData(T data) {
        this.detail = data;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public T getData() {
        return detail;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}

