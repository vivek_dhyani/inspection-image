package com.cars24.inspection.util;


import javax.validation.constraints.Null;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class DateUtil {


    /**
     * Date time.
     *
     * @return the local date time
     */
    public static String getCurrentTime(Date date, @Null String dateFormat) {
        if(dateFormat == null) {
            dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        }
        SimpleDateFormat dateFormatUtc = new SimpleDateFormat(dateFormat);
        dateFormatUtc.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormatUtc.format(date);
    }

    public static LocalDateTime getDateFromGivenString(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return LocalDateTime.parse(date, formatter);
    }
    /**
     * Date time.
     *
     * @return the local date time
     */
    public static LocalDateTime getCurrentTime() {
        return LocalDateTime.now().plusHours(5).plusMinutes(30);
    }

    /**
     *
     * @param miliSeconds
     * @param dateFormat
     * @return
     */
    public static String getFormattedTime(long miliSeconds, @Null String dateFormat) {
        if(dateFormat == null) {
            dateFormat = "yyyy-MM-dd HH:mm:ss.SSS";
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliSeconds);
        Date date = new Date(miliSeconds);
        SimpleDateFormat sd = new SimpleDateFormat(dateFormat);
        return sd.format(date);
    }

    /**
     * function to get Date Time form timestamp
     * @param miliSeconds
     * @return
     */
    public static LocalDateTime getDateFromTimeStamp(long miliSeconds) {
        return  LocalDateTime.ofEpochSecond(miliSeconds, 0, ZoneOffset.UTC);
    }


    /**
     *function to get current timestamp in sec
     *@return sec in long
     */
    public static long getTime(){
        return System.currentTimeMillis() / 1000;
    }
}
