package com.cars24.inspection.util;

import javax.validation.constraints.Null;
import java.lang.reflect.Field;

public class ReflectionUtil {

    /**
     * function to get property attribte of an object
     * @param object
     * @param key
     * @return
     */
    public Object getObjAttribute(Object object, String key, @Null Boolean createNewInstance) {
        try {
            if (object != null && !key.isEmpty()) {
                Field attributesField = object.getClass().getField(key);
                if (attributesField != null) {
                    Object property = attributesField.get(object);
                    if(createNewInstance !=null && createNewInstance && property == null){
                        property = attributesField.getType().newInstance();
                        attributesField.set(object, property);
                    }
                    return  property;
                }
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
