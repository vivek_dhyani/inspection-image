package com.cars24.inspection.util;

import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;


@Service
public class ContextUtils implements ApplicationContextAware {
    private static ApplicationContext CONTEXT;
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        CONTEXT = context;
    }
    public static <T> T getBean(Class<T> beanClass) {
        return CONTEXT.getBean(beanClass);
    }
    public static String activeProfile() {
        return CONTEXT.getEnvironment().getActiveProfiles() != null
                && CONTEXT.getEnvironment().getActiveProfiles().length > 0
                ? CONTEXT.getEnvironment().getActiveProfiles()[0]
                : "prod";
    }
}
