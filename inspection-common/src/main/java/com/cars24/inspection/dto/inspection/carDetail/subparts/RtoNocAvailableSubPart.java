package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;


/**
 * @author Amrit Kumar on 21/2/19.
 */
public class RtoNocAvailableSubPart extends SubAttributes  {
   public RtoNocIssuedForAttributes rtoNocIssuedFor;
   public RtoNocValidityAttribute rtoNocValidity;
}
