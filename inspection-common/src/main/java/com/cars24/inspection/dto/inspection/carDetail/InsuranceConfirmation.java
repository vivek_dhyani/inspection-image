package com.cars24.inspection.dto.inspection.carDetail;

import com.cars24.inspection.dto.inspection.carDetail.attributes.InsuranceTypeAttributes;




/**
 * @author Amrit.Chakradhari
 * This class is create to hold the data form InsuranceConfirmationFragment Screen
 */
public class InsuranceConfirmation  {
    public InsuranceTypeAttributes insuranceType;
}
