package com.cars24.inspection.dto.inspection.exteriortyre.subattributes;


import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;

public class CrossMemberSubAttributes extends SubAttributes  {
    public SubAttribute upper;
    public SubAttribute lower;
}
