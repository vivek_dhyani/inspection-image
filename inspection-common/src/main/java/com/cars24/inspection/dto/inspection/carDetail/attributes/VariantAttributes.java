package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;




/**
 * @author Saunik Singh on 10/9/18.
 * Cars24 Services Private Limited.
 */
public class VariantAttributes extends Attributes  {
    public String transportation_category;
}
