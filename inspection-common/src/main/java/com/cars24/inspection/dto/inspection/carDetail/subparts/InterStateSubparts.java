package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.carDetail.attributes.InterstateNocIssuedAttributes;
import com.cars24.inspection.dto.inspection.carDetail.attributes.RoadTaxPaidInterStateAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 17-01-2018.
 */

public class InterStateSubparts extends SubAttributes  {
    public InterstateNocIssuedAttributes interstateNocIssued;
    public RoadTaxPaidInterStateAttribute roadTaxPaidInterstate;
}
