package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.ExtendedWarrantyAvailableSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * @author  Amrit.Chakradhari 2020-07-09
 */
public class ExtendedWarrantyAvailableAttributes extends Attributes  {
    public ExtendedWarrantyAvailableSubParts subParts;
}
