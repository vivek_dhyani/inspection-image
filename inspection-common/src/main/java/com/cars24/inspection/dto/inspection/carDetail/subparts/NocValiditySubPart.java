package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Praveen.Sharma on 8/14/2018.
 */
public class NocValiditySubPart extends SubAttributes  {
    public SubAttribute nocValidityDate;
}
