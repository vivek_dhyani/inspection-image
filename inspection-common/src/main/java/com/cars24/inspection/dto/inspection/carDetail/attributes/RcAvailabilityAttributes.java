package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.RcAvailabilitySubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;


/**
 * @author Saunik on 20-11-2017.
 */

public class RcAvailabilityAttributes extends Attributes  {
    public RcAvailabilitySubParts subParts;
}
