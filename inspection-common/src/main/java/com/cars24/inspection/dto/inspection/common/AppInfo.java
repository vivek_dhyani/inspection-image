package com.cars24.inspection.dto.inspection.common;

import java.io.Serializable;

public class AppInfo implements Serializable
{
    private String role;
    private String version;
    private String inspectionStatus;
    private String deviceName;
    private String osVer;
    public String uniqueID;
    public  AppInfo()
    {
        role="";
        version="";
        inspectionStatus="";
        deviceName="";
        osVer="";
        uniqueID = "";
    }
    public String getVersion() {
        return version;
    }
    public void setVersion(String version) {
        this.version = version;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public String getInspectionStatus() {
        return inspectionStatus;
    }
    public void setInspectionStatus(String inspectionStatus) {
        this.inspectionStatus = inspectionStatus;
    }
    public String getDeviceName() {
        return deviceName;
    }
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
    public String getOsVer() {
        return osVer;
    }
    public void setOsVer(String osVer) {
        this.osVer = osVer;
    }
}