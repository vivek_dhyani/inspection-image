package com.cars24.inspection.dto.inspection.common;

///import com.cars24.inspection.constants.IConstant;
import lombok.Data;
import java.util.ArrayList;

@Data
public class ImageWebModel {
    public String title;
    public String hashKey;
    public String field;
    public String url;
    public String originalUrl;
    public String thumbUrl;
    public String type;
    public String carId;
    public int syncState; // refer to IConstants sync states.
    public int deleteState;
    public boolean showError;
    public String section;
    public String role;
    public boolean isOffline = false;
    public String appointmentId;
    public String inspectionStatus;
    public String subPart;
    public String subSubPart;
    public String part;
    public String errorMessage;
    public String uploadTime;
    public String subSubPartTitle;
    public String subSubPartValue;
    public String partTitle;
    public String partValue;
    public int compressionState;
    public int pointerCount;
    public int isTilted;
    public transient boolean isFileUploading = false;
    public transient boolean isServerError = false;
    public transient boolean isImageReview = false;
    //public transient String imageUrlWithMarker = IConstant.EMPTY;
    public String fieldKey;
    public int blurScore = -1;
    public ArrayList<DamageMarker> markers = null;
    public String subPartTitle;
    public String subPartValue;

    public ImageWebModel() {
        //this.syncState = IConstant.IMAGE_SYNC_STATE_NOT_STARTED;
        //this.compressionState = IConstant.IMAGE_SYNC_STATE_COMPRESSION_NOT_DONE;
        //this.deleteState = IConstant.IMAGE_STATE_NOT_DELETING;
    }

    public String getObjectHashCode(){
        return String.format("%1s-%2s-%3s",this.carId,this.hashKey,this.url);
    }
}
