package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;

/**
 * Created by Praveen.Sharma on 2019-12-31.
 */
public class FuelTypeSubParts extends SubAttributes {
    public SubAttribute cngLpgFitment;
}
