package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.carDetail.attributes.NoClaimBonusAttributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class InsuranceTypeSubParts extends SubAttributes  {
    public SubAttribute insuranceExpiry;
    public NoClaimBonusAttributes noClaimBonus;
    public SubAttribute mismatchInInsurance;
}
