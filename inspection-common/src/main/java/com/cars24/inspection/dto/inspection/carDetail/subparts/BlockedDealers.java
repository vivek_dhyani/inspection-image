package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;

public class BlockedDealers extends SubAttribute {
    public int dealerId;
    public DealersSubParts subParts;
}
