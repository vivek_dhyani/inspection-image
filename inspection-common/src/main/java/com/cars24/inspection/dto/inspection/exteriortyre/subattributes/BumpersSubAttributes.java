package com.cars24.inspection.dto.inspection.exteriortyre.subattributes;


import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;
import lombok.Data;

@Data
public class BumpersSubAttributes extends SubAttributes  {
    public SubAttribute rear;
    public SubAttribute front;
}
