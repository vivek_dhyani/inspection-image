package com.cars24.inspection.dto.inspection.common.attributes;


import com.cars24.inspection.dto.inspection.common.ParentAttribute;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Attributes extends ParentAttribute {

    @JsonProperty("subParts")
    public Map<String, Attributes> subParts;

}
