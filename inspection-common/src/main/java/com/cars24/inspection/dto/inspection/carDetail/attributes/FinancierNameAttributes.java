package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;

/**
 * Created by Praveen.Sharma on 13/08/20.
 */
public class FinancierNameAttributes extends Attributes {
    public int nocClearanceTime;
}
