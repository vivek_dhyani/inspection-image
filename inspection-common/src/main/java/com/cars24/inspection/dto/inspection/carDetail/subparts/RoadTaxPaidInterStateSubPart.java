package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.carDetail.attributes.RoadTaxValidityAttributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Amrit.Kumar on 12/26/2017.
 */

public class RoadTaxPaidInterStateSubPart extends SubAttributes  {
  public  RoadTaxValidityAttributes roadTaxValidity;
}
