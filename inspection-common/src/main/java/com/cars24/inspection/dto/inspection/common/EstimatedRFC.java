package com.cars24.inspection.dto.inspection.common;

import lombok.Data;
@Data
public class EstimatedRFC {
    public long value;
    public String title;
}
