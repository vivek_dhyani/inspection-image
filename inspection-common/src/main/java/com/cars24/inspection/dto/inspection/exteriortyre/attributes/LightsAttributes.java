package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.LightsSubAttributes;
import com.cars24.inspection.util.ReflectionUtil;
import lombok.Data;

@Data
public class LightsAttributes extends Attributes  {
    public LightsSubAttributes subParts;

}
