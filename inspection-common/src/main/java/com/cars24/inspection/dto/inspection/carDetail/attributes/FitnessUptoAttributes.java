package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.FitnessUptoSupbarts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;

public class FitnessUptoAttributes extends Attributes {
    public FitnessUptoSupbarts subParts;
}
