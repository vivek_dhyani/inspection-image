package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.ApronSubAttributes;
import com.cars24.inspection.util.ReflectionUtil;


public class ApronAttributes extends Attributes  {
    public ApronSubAttributes subParts;
}
