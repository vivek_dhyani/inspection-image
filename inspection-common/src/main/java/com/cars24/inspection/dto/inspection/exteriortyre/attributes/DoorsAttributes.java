package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.DoorsSubAttributes;

public class DoorsAttributes extends Attributes  {
    public DoorsSubAttributes subParts;
}
