package com.cars24.inspection.dto.inspection.ssb;
import com.cars24.inspection.dto.inspection.common.Comments;
import com.cars24.inspection.dto.inspection.common.EstimatedRFC;
import com.cars24.inspection.dto.inspection.common.Rating;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import lombok.Data;

@Data
public class SteeringSuspensionBrakes {
    public Attributes steering;
    public Attributes suspension;
    public Attributes brakes;
    public EstimatedRFC estimatedRFC;
    public Rating rating;
    public Comments comments;
    public Comments highlightPositives;
    public Attributes rejectionRemark;
}
