package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.carDetail.attributes.RcConditionAttributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;


import java.util.Map;

/**
 * @author Saunik on 20-11-2017.
 */

public class RcAvailabilitySubParts extends SubAttributes  {
    public SubAttribute rcAtDelivery;
    public RcConditionAttributes rcCondition;
}
