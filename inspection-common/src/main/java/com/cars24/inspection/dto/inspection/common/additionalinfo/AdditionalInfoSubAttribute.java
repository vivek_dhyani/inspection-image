package com.cars24.inspection.entity.inspection.common.additionalinfo;


import java.util.List;
import lombok.Data;

@Data
public class AdditionalInfoSubAttribute extends AdditionalInfoSubAttributeParent{
    public String title;
    public List<String> value;

    public AdditionalInfoSubAttribute(String title, List<String> value){
        this.title = title;
        this.value = value;
    }
}
