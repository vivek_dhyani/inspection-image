package com.cars24.inspection.dto;

public class BaseRequest {

    private String role;
    private String user;
    private String inspectionStatus;
    public String appointmentId;
    public String inspectionStartTime;
    public String reviewStartTime;
    public String sectionEndTime;
    private String client;

    public String getRole() {

        return role;
    }
    public void setRole(String role) {

        this.role = role;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getInspectionStatus() {
        return inspectionStatus;
    }

    public void setInspectionStatus(String inspectionStatus) {
        this.inspectionStatus = inspectionStatus;
    }

    public String getClient() {

        return client;
    }
    public void setClient(String client) {

        this.client = client;
    }
}
