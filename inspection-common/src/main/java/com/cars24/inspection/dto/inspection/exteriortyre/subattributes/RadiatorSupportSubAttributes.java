package com.cars24.inspection.dto.inspection.exteriortyre.subattributes;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;

public class RadiatorSupportSubAttributes extends SubAttributes  {
    public SubAttribute lhs;
    public SubAttribute rhs;
    public SubAttribute upper;
    public SubAttribute lower;
}
