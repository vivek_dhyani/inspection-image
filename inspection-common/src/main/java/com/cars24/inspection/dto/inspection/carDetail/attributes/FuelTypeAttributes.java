package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.FuelTypeSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;

/**
 * Created by Praveen.Sharma on 2019-12-31.
 */
public class FuelTypeAttributes extends Attributes {
   public FuelTypeSubParts subParts;

}
