package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * @author Praveen.Sharma on 8/14/2018.
 */
public class InterStateNocIssuedForAttribute extends Attributes  {
    public NocValiditySubPart subParts;
}
