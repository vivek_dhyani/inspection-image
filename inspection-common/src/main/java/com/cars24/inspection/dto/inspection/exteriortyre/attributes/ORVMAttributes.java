package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.ORVMSubAttributes;

public class ORVMAttributes extends Attributes  {
    public ORVMSubAttributes subParts;
}
