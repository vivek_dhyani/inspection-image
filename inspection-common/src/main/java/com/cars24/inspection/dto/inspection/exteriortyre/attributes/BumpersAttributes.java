package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.BumpersSubAttributes;
import lombok.Data;

@Data
public class BumpersAttributes extends Attributes  {
    public BumpersSubAttributes subParts;
}
