package com.cars24.inspection.dto.inspection.exteriortyre.subattributes;


import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;
import lombok.Data;

@Data
public class FendersSubAttributes extends SubAttributes  {
    public SubAttribute lhs;
    public SubAttribute rhs;
}
