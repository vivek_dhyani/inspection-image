package com.cars24.inspection.dto.inspection.exteriortyre;

import com.cars24.inspection.dto.inspection.common.*;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.exteriortyre.attributes.*;
import lombok.Data;

import java.util.ArrayList;

@Data
public class ExteriorTyres {
    public BumpersAttributes bumpers;
    public Attributes bonnetHood;
    public Attributes roof;
    public FendersAttributes fenders;
    public DoorsAttributes doors;
    public PillarsAttributes pillars;
    public RunningBorderAttributes runningBorder;
    public QuarterPanelAttributes quarterPanel;
    public Attributes dickyDoorBootDoor;
    public Attributes bootFloor;
    public ApronAttributes apron;
    public Attributes firewall;
    public Attributes cowlTop;
    public Attributes lowerCrossMember;
    public Attributes bonnetPatti;
    public Attributes headLightSupport;
    public Attributes radiatorSupport;
    public Attributes frontShow;
    public WindshieldAttributes windshield;
    public ORVMAttributes orvm;
    public LightsAttributes lights;
    public Attributes alloyWheels;
    public Attributes lhsFrontTyre;
    public Attributes rhsFrontTyre;
    public Attributes lhsRearTyre;
    public Attributes rhsRearTyre;
    public Attributes spareTyre;
    public ArrayList<ImageWebModel> mainImages;
    public ArrayList<ImageWebModel> mainVideos;
    public EstimatedRFC estimatedRFC;
    public Rating rating;
    public Comments comments;
    public Comments highlightPositives;
    public Attributes rejectionRemark;

}
