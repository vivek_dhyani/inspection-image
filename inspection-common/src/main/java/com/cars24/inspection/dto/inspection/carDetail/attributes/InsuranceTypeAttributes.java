package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.InsuranceTypeSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class InsuranceTypeAttributes extends Attributes  {
    public InsuranceTypeSubParts subParts;
}
