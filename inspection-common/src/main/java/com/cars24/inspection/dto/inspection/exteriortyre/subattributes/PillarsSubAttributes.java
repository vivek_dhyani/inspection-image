package com.cars24.inspection.dto.inspection.exteriortyre.subattributes;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;

public class PillarsSubAttributes extends SubAttributes  {
    public SubAttribute lhsA;
    public SubAttribute rhsA;
    public SubAttribute lhsB;
    public SubAttribute rhsB;
    public SubAttribute lhsC;
    public SubAttribute rhsC;
}
