package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.IsUnderHypothecationSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;


/**
 * @author Saunik on 14-11-2017.
 */

public class IsUnderHypothecationAttributes extends Attributes  {
    public IsUnderHypothecationSubParts subParts;
}
