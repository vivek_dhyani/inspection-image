package com.cars24.inspection.dto.inspection.exteriortyre.subattributes;


import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;

public class TyreRimSubAttributes extends SubAttributes  {
    public SubAttribute lhsFront;
    public SubAttribute rhsFront;
    public SubAttribute lhsRear;
    public SubAttribute rhsRear;
    public SubAttribute spare;
}
