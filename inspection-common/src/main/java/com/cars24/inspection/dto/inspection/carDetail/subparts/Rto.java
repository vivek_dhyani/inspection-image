package com.cars24.inspection.dto.inspection.carDetail.subparts;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;


import java.util.ArrayList;

public class Rto extends SubAttribute  {
    public String rtoCode;
    public ArrayList<String> suggestedValues;
}
