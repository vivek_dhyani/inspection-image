package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class TPermitTypeSubParts extends SubAttributes  {
    public SubAttribute tPermitExpiryDate;
}
