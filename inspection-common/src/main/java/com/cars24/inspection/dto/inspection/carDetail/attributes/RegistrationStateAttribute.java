package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.RegistrationStateSubPart;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



public class RegistrationStateAttribute extends Attributes  {
    public RegistrationStateSubPart subParts;
}
