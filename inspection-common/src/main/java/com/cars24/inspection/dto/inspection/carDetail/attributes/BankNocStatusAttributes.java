package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.BankNocStatusSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;

public class BankNocStatusAttributes extends Attributes {
    public BankNocStatusSubParts subParts;
}
