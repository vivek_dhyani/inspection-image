package com.cars24.inspection.dto.inspection;


import com.cars24.inspection.dto.BaseRequest;
import com.cars24.inspection.dto.inspection.carDetail.CarDetails;
import com.cars24.inspection.dto.inspection.common.AppInfo;
import com.cars24.inspection.dto.inspection.exteriortyre.ExteriorTyres;
import com.cars24.inspection.dto.inspection.ssb.SteeringSuspensionBrakes;
import lombok.Data;


import java.util.ArrayList;
@Data
public class InspectionObject extends BaseRequest {
   /*
    public ElectricalInterior electricalsInterior=null;
    public EngineTransmission engineTransmission=null;
    public SteeringSuspensionBrakes steeringSuspensionBrakes=null;
    public AirConditioning airConditioning=null;
    public Summary summary=null;
    */
    public CarDetails carDetails=null;
    public SteeringSuspensionBrakes steeringSuspensionBrakes=null;
    public ExteriorTyres exteriorTyres=null;
    //public AppDetailsAttributes[] appDetails = null;

    public ArrayList<AppInfo> appDetails;
    public String inspectionStartTime;
    public String sectionEndTime;
}
