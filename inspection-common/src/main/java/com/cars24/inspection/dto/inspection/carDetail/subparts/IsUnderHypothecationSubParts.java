package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.carDetail.attributes.BankNocStatusAttributes;
import com.cars24.inspection.dto.inspection.carDetail.attributes.FinancierNameAttributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Saunik on 14-11-2017.
 */

public class IsUnderHypothecationSubParts extends SubAttributes  {
    public BankNocStatusAttributes bankNocStatus;
    public FinancierNameAttributes financierName;
}
