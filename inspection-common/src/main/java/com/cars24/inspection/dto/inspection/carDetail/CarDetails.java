package com.cars24.inspection.dto.inspection.carDetail;

import com.cars24.inspection.dto.inspection.carDetail.attributes.*;
import com.cars24.inspection.dto.inspection.common.BasePojo;
import com.cars24.inspection.dto.inspection.common.ImageWebModel;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;

import java.util.ArrayList;

public class CarDetails extends BasePojo {
    public Attributes appointmentId;
    public Attributes city;
    public Attributes inspectionAt;
    public StoreNameAttribute storeName;
    public Attributes make;
    public Attributes model;
    public Attributes year;
    public Attributes month;
    public VariantAttributes variant;
    public Attributes registrationYear;
    public Attributes registrationMonth;
    public Attributes fitnessUpto;
    public RegistrationStateAttribute registrationState;
    public Attributes registrationCity;
    public Attributes registrationNumber;
    public Attributes ownerNumber;
    public Attributes toBeScrapped;
    public Attributes duplicateKey;
    public FuelTypeAttributes fuelType;
    public Attributes cngLpgFitment;
    public InsuranceTypeAttributes insuranceType;
    public Attributes customerName;
    public ArrayList<ImageWebModel> chassisNumberImage;
    public ChassisNumberAttribute chassisNumber;
    public Attributes chassisNumberEmbossing;
    public RcAvailabilityAttributes rcAvailability;
    public RoadTaxPaidAttributes roadTaxPaid;
    public InterStateAttributes interState;
    public RtoNocIssuedAttributes rtoNocIssued;
    public IsUnderHypothecationAttributes isUnderHypothecation;
    public RADetailsAttributes raDetails;
    public ExtendedWarrantyAvailableAttributes extendedWarranty;
    public ArrayList<ImageWebModel> warrantyImages;
    public ArrayList<ImageWebModel> rcImages;
    public ArrayList<ImageWebModel> insuranceImages;
    public ArrayList<ImageWebModel> additionalImages;
    public ArrayList<ImageWebModel> hypothecationImages;
    public Attributes insuranceNudge;
    public LocationData geoLocation;
    public Attributes distanceTraveled;
    public Attributes partiPeshi;
}
