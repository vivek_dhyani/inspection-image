package com.cars24.inspection.dto.inspection.common;

import java.util.ArrayList;
import lombok.Data;

@Data
public class Comments {
    public String title;
    public ArrayList<Comment> value;
}
