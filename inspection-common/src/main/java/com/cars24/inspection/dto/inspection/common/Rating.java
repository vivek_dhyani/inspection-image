package com.cars24.inspection.dto.inspection.common;

import lombok.Data;
public class Rating{
    public float value;
    public String title;
}

