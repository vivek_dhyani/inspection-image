package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.NoClaimBonusSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * Created by Praveen.Sharma on 2019-07-29.
 */
public class NoClaimBonusAttributes extends Attributes   {
    public NoClaimBonusSubParts subParts;
}
