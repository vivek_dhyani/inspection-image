package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.RtoNocIssuedSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * @author Amrit Kumar on 21/2/19.
 */
public class RtoNocIssuedAttributes extends Attributes  {
    public RtoNocIssuedSubParts subParts;
}
