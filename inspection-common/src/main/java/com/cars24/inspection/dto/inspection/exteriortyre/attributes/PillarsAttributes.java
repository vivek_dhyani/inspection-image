package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.PillarsSubAttributes;
import com.cars24.inspection.util.ReflectionUtil;


public class PillarsAttributes extends Attributes {
    public PillarsSubAttributes subParts;
}
