package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * Created by Praveen.Sharma on 2019-11-07.
 */
public class ChassisNumberAttribute  extends Attributes  {
   public transient String lastFiveChar;
   public transient String firstChars;
}
