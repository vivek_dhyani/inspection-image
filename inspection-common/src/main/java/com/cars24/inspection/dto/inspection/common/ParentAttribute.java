package com.cars24.inspection.dto.inspection.common;

import com.cars24.inspection.entity.inspection.common.additionalinfo.AdditionalInfo;
//import com.cars24.inspection.entity.inspection.common.ImageWebModel;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import org.jboss.logging.Field;

@Data
public class ParentAttribute{
    public String value;
    public String id;
    public ArrayList<AdditionalInfo> additionalInfo;
   // public ImageWebModel image;
    public String title;
    public String aliasValue;
}
