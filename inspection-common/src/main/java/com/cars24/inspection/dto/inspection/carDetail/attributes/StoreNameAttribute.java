package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.StoreNameSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * @author Saunik on 04-12-2017.
 */

public class StoreNameAttribute extends Attributes  {
    public StoreNameSubParts subParts;
    public String regionId;
    public String regionName;
}
