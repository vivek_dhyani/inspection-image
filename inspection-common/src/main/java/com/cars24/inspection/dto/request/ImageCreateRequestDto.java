package com.cars24.inspection.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.web.multipart.MultipartFile;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageCreateRequestDto {

    @NotNull(message = "carId must not be empty")
    private Integer carId;
    private String id;
    private String aliasValue;
    private String role;
    private String type;
    private String inspectionStatus;
    private String title;
    private String field;
    private String section;
    private String hashKey;
    private String part;
    private String partTitle;
    private String partValue;
    private String subPart;
    private String subPartTitle;
    private String subPartValue;
    private String subSubPart;
    private String subSubPartTitle;
    private String subSubPartValue;
    private Map<String, List<String>> currentCondition;
    private Map<String, List<String>> workDone;
    private Map<String, List<String>> treadDepth;
    private Long uploadTime;
    private MultipartFile imageFile;
    private MultipartFile videoFile;
    private MultipartFile thumbFile;
    private String imageUrl;
    private String videoUrl;
    private String thumbUrl;
    private Integer isTilted;
    private Integer pointerCount;
}
