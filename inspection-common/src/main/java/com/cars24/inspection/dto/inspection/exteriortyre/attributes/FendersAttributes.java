package com.cars24.inspection.dto.inspection.exteriortyre.attributes;

import com.cars24.inspection.dto.inspection.common.attributes.Attributes;
import com.cars24.inspection.dto.inspection.exteriortyre.subattributes.FendersSubAttributes;

public class FendersAttributes extends Attributes  {
    public FendersSubAttributes subParts;
}
