package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.carDetail.attributes.RtoNmcAvailableAttributes;
import com.cars24.inspection.dto.inspection.carDetail.attributes.RtoNocAvailableAttributes;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;



/**
 * @author Amrit Kumar on 21/2/19.
 */
public class RtoNocIssuedSubParts extends SubAttributes  {
    public RtoNocAvailableAttributes rtoNocAvailable;
    public RtoNocIssuedForAttributes rtoNocIssuedFor;
    public RtoNocIssuedDateAttribute rtoNocIssuedDate;
    public RtoNmcAvailableAttributes rtoNmcAvailable;
}
