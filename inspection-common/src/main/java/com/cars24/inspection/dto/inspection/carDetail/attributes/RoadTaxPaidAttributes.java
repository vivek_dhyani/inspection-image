package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.RoadTaxPaidSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



public class RoadTaxPaidAttributes extends Attributes  {
    public RoadTaxPaidSubParts subParts;
}
