package com.cars24.inspection.dto.inspection.carDetail.subparts;

import com.cars24.inspection.dto.inspection.common.subparts.SubAttribute;
import com.cars24.inspection.dto.inspection.common.subparts.SubAttributes;

public class FitnessUptoSupbarts extends SubAttributes {
   public SubAttribute toBeScrapped;
}
