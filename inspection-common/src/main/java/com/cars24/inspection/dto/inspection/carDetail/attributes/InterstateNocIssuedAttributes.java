package com.cars24.inspection.dto.inspection.carDetail.attributes;

import com.cars24.inspection.dto.inspection.carDetail.subparts.InterstateNocIssuedSubParts;
import com.cars24.inspection.dto.inspection.common.attributes.Attributes;



/**
 * @author Saunik on 20-11-2017.
 */

public class InterstateNocIssuedAttributes extends Attributes  {
    public InterstateNocIssuedSubParts subParts;
}

