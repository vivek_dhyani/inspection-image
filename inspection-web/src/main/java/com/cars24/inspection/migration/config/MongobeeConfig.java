package com.cars24.inspection.migration.config;

import com.github.mongobee.Mongobee;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

/**
 * simple MongoBee configuration. set Database URI for MongoDB Source. set
 * Database Name for migration operation. set BasePath to ChangeLog class, for
 * definition of changes.
 */
@Configuration
public class MongobeeConfig extends AbstractMongoConfiguration {

	/** The database name. */
	private final String databaseName;
	
	/** The database uri. */
	private final String databaseUri;
	
	/** The change log path. */
	private final String changeLogPath;

	/**
	 * Instantiates a new mongobee config.
	 *
	 * @param databaseName the database name
	 * @param databaseUri the database uri
	 * @param changeLogPath the change log path
	 */
	@Autowired
	public MongobeeConfig(@Value("${spring.data.mongodb.database}") String databaseName, @Value("${spring.data.mongodb.uri}") String databaseUri, @Value("${application.property.mongobee.changelog.path}") String changeLogPath) {
		this.databaseName = databaseName;
		this.databaseUri = databaseUri;
		this.changeLogPath = changeLogPath;
	}

	/* (non-Javadoc)
	 * @see org.springframework.data.mongodb.config.AbstractMongoConfiguration#mongoClient()
	 */
	@Override
	public MongoClient mongoClient() {
		return new MongoClient(new MongoClientURI(databaseUri));
	}

	/* (non-Javadoc)
	 * @see org.springframework.data.mongodb.config.MongoConfigurationSupport#getDatabaseName()
	 */
	@Override
	protected String getDatabaseName() {
		return databaseName;
	}

	/**
	 * Mongobee.
	 *
	 * @return the mongobee
	 * @throws Exception the exception
	 */
	@Bean
	public Mongobee mongobee() throws Exception {
		Mongobee runner = new Mongobee(databaseUri);
		runner.setDbName(databaseName);
		runner.setChangeLogsScanPackage(changeLogPath); 
		runner.setEnabled(true);
		runner.setMongoTemplate(mongoTemplate());
		return runner;
	}

}