package com.cars24.inspection.migration.changelog;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import java.util.List;

/**
 * The Class DatabaseChangeLog.
 */
@ChangeLog
public class DatabaseChangeLog {


	/**
	 * Adds the attribute config.
	 *
	 * @param db the db
	 */
	@ChangeSet(order = "001", id = "addAttributeConfig", author = "priyanka.singh@cars24.com", runAlways = false)
	public void addAttributeConfig(final DB db) {
		String json = "[{'key':'variantyearId','value':'variantyearId'},{'key':'fuelType','value':'Fuel Type'},{'key':'outsideDoorHandleColour','value':'Outside Door Handle Colour'},{'key':'cubicCapacity','value':'Cubic Capacity'},{'key':'transmission','value':'Transmission'},{'key':'absAntiLockBrakingSystem','value':'ABS Anti Lock Braking System'},{'key':'airBag','value':'Airbags'},{'key':'centralLocking','value':'Central Locking'},{'key':'acClimateControl','value':'Ac Climate Control'},{'key':'cruiseControl','value':'Cruise Control'},{'key':'electronicStabilityControl','value':'ESP'},{'key':'fogLamps','value':'Fog Lamps'},{'key':'gloveBoxLamp','value':'Glove Box Lamp'},{'key':'hillHoldControl','value':'HHC'},{'key':'keylessEntry','value':'Keyless Entry'},{'key':'leatherSeats','value':'Leather Seats'},{'key':'mechanicalBrakeAssist','value':'MBA'},{'key':'navigation','value':'Navigation'},{'key':'paddleShifter','value':'Paddle Shifter'},{'key':'parkAssist','value':'Park Assist'},{'key':'parkingCameras','value':'Parking Cameras'},{'key':'parkingSensors','value':'Parking Sensors (F/R)'},{'key':'powerWindows','value':'Power Windows (Front / Rear)'},{'key':'rearCentreArmrest','value':'Rear Centre Armrest'},{'key':'rearDefogger','value':'Rear Defogger'},{'key':'rearWashWiper','value':'Rear Wash Wiper'},{'key':'remoteLocking','value':'Remote Locking'},{'key':'sunroof','value':'Sunroof'},{'key':'touchscreenAudio','value':'Touchscreen Audio'},{'key':'tirePressureMonitoring','value':'Trip Computer'},{'key':'tripComputer','value':'Trip Computer'},{'key':'wheelCover','value':'Wheel Cover'},{'key':'rearSeatSplit6040','value':'Rear Seat Split 60:40'},{'key':'reverseCameraOemFitment','value':'Reverse Camera OEM Fitment'},{'key':'steeringAdjustment','value':'Steering Adjustment'},{'key':'steeringMultiFunctionalConrol','value':'Steering Multi Functional Conrol'},{'key':'alloyWheels','value':'Alloy Wheels'}]";
		DBCollection collection = db.getCollection("attribute_config");
		@SuppressWarnings({ "unchecked", "deprecation" })
		List<DBObject> dbObject = (List<DBObject>) JSON.parse(json);
		collection.insert(dbObject);
	}

}
