 package com.cars24.inspection.controller;

import com.cars24.inspection.ApiResponse;
import com.cars24.inspection.constants.ConstantFields;
import com.cars24.inspection.util.ReflectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import com.cars24.inspection.service.ImageService;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import com.cars24.inspection.entity.inspection.Inspection;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import com.cars24.inspection.repository.InspectionRepository;

import java.util.*;

import static org.springframework.util.StringUtils.isEmpty;

@RestController
public class ImageController {

    /** The Constant logger. */
    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    private static final String[]  VALID_MASK_UTM_SOURCES = {"qc", "cjedit"};

    @Autowired
    private ImageService imgService;

    @Autowired
    private MongoTemplate mongoAdapter;

    @Autowired
    private InspectionRepository insRep;

    @PostMapping("/v2/image")
    public ResponseEntity<ApiResponse<Object>> imageUpload(@RequestHeader HttpHeaders headers, @ModelAttribute ImageCreateRequestDto imageDto, @RequestParam(required = false) String utm_source , Errors errors) {
        Map<String, Object> details = new HashMap<String, Object>();
        try {
            imgService.validateAuthorization(headers.get("Authorization"));
            final Query query = new Query();
            Update update = new Update();
            query.addCriteria(Criteria.where("carId").is(imageDto.getCarId()));
            Inspection insData = mongoAdapter.findOne(query, Inspection.class);
            if (insData == null ||  insData.getAppointmentId() == null) {
                return ApiResponse.buildWithError(422, "AppointmentId Not found", "Inspection record updation failed", "Inspection data Update", null);
            }
            if (!this.roleValidation(imageDto.getRole(), insData.getInspectionStatus(), imageDto.getInspectionStatus())) {
                return ApiResponse.buildWithError(422, "This role is not permitted to update inspection!", "Inspection record updation failed", "Inspection data Update", null);
            }

            imgService.uploadFile(imageDto, insData);
            imgService.prepareAndpushPlateMaskingToDS(imageDto, insData);
            if (Arrays.asList(VALID_MASK_UTM_SOURCES).contains(utm_source)) {
                Map<String, String> maskResponse = this.imgService.setMaskedData(imageDto, insData, utm_source);
                if (!maskResponse.isEmpty()) {
                    details.putAll(maskResponse);
                    String fieldToupdate = "inspectionData." + imageDto.getSection() + ".";
                    String fieldPart = (imageDto.getType().equals(ConstantFields.AdditionalInfo)) ? imageDto.getPart() : imageDto.getType();
                    fieldToupdate += fieldPart;
                    ReflectionUtil refUtil = new ReflectionUtil();
                    Object section = refUtil.getObjAttribute(insData.getInspectionData(), imageDto.getSection(), true);
                    Object part = refUtil.getObjAttribute(section, fieldPart, true);
                    update.set(fieldToupdate, part);
                    mongoAdapter.findAndModify(query, update, Inspection.class);
                    return ApiResponse.buildWithSuccess(200, details, "Image Uploading respons", "Image Uploading response", null);
                }
            }

            // Update update = new Update();
            Object attr = insData.getInspectionData().setImageData(imageDto);
            String fieldToupdate = "inspectionData." + imageDto.getSection() + ".";
            fieldToupdate += (imageDto.getType().equals(ConstantFields.AdditionalInfo)) ? imageDto.getPart() : imageDto.getType();
            update.set(fieldToupdate, attr);
            mongoAdapter.findAndModify(query, update, Inspection.class);

            details.put("carId", imageDto.getCarId());
            if (imageDto.getImageUrl() != null || imageDto.getVideoUrl() != null) {
                String url = (imageDto.getImageUrl() != null) ? imageDto.getImageUrl() : imageDto.getVideoUrl();
                details.put("url", url);
            }
            if (imageDto.getThumbUrl() != null) {
                details.put("thumbUrl", imageDto.getThumbUrl());
            }
            return ApiResponse.buildWithSuccess(200, details, "Image Uploading respons", "Image Uploading response", null);
        }catch (Exception e){
            Integer statusCode = 422;
            System.out.println("error Msg :"+e.getMessage());
            e.printStackTrace();
            String[] message = e.getMessage().split("-");
            statusCode = (message[1] == null || isEmpty(message[1])) ? 422 : Integer.valueOf(message[1]);
            return ApiResponse.buildWithError(statusCode, message[0], "Inspection record updation failed", "Inspection data Update", null);
        }
    }


    private boolean roleValidation(String role, String currentStatus, String targetStatus) {
        Map status = new HashMap();
        Map inspectionStatusMapping = new HashMap();
        ArrayList<String> newStatus = new ArrayList<String>();
        newStatus.add("new");
        newStatus.add("smPending");
        ArrayList<String> smPendingStatus = new ArrayList<String>();
        smPendingStatus.add("smPending");
        ArrayList<String> smReviewRejected = new ArrayList<String>();
        smReviewRejected.add("smReviewRejected");
        inspectionStatusMapping.put("new", newStatus);
        inspectionStatusMapping.put("smPending", smPendingStatus);
        inspectionStatusMapping.put("smReviewRejected", smReviewRejected);
        status.put("store manager", inspectionStatusMapping);
        return ((ArrayList)((Map) status.get(role)).get(currentStatus)).contains(targetStatus);
    }
}
