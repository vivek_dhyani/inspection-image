package com.cars24.inspection.service;

import com.cars24.inspection.entity.InsPlateMaskingLog;
import com.cars24.inspection.entity.inspection.Inspection;
import org.junit.jupiter.api.*;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.cars24.inspection.dto.request.ImageCreateRequestDto;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@RunWith(SpringRunner.class)
@SpringBootTest
class ImageServiceTest {

    @Autowired
    private ImageService imgService;

    @Autowired
    private MongoTemplate mongoAdapter;

    @Autowired
    private S3Service s3Adaptor;
    //carDetails_duplicateKey_additionalInfo_form_0 , inspectionData_exteriorTyres_mainImages_frontMain
    String addtionalInfoRequest = "{\"carId\": 6126700,\"section\": \"carDetails\",\"type\": \"additionalInfo\",\"title\": \"Duplicate Key\",\"part\": \"duplicateKey\",\"partTitle\": \"Duplicate Key\",\"partValue\": \"Yes\",\"hashKey\": \"carDetails_duplicateKey_additionalInfo_form_0\",\"role\": \"store manager\",\"inspectionStatus\":\"smPending\"}";
    ImageCreateRequestDto imgReqDto;
    Inspection insData;

    @BeforeEach
    void setUp() {
        try {
            this.imgReqDto = new ObjectMapper().readValue(addtionalInfoRequest, ImageCreateRequestDto.class);
            MockMultipartFile file = new MockMultipartFile(
                    "imageFile",
                    "dummy.png",
                    "image/png",
                    Files.readAllBytes(Paths.get("/var/www/html/dummy.png"))
            );
            this.imgReqDto.setImageFile(file);
            final Query query = new Query();
            query.addCriteria(Criteria.where("carId").is(imgReqDto.getCarId()));
            this.insData = mongoAdapter.findOne(query, Inspection.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void uploadFile() {
        try {
            imgService.uploadFile(imgReqDto, insData);
            String fileName = imgReqDto.getImageUrl();
            Boolean fileExist = s3Adaptor.fileExists(fileName, false);
            Assertions.assertEquals(true, fileExist);
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    @Test
    void prepareAndpushPlateMaskingToDS() {
        Boolean dataPushed = imgService.prepareAndpushPlateMaskingToDS(imgReqDto, insData);
        if (dataPushed) {
            final Query query = new Query();
            query.addCriteria(Criteria.where("appointmentId").is(insData.getAppointmentId()));
            query.addCriteria(Criteria.where("request.$.url").is(imgReqDto.getImageUrl()));
            InsPlateMaskingLog data = mongoAdapter.findOne(query, InsPlateMaskingLog.class);
            Assertions.assertNotNull(data);
        }
    }

    @Test
    void setMaskedData(){
        imgReqDto.setImageUrl("inspection/2020/04/27/CCXR6X5/v0/carDetails/additionalInfo/duplicateKey/duplicatekey_1602684883376.png");
        Map<String, String> maskResponse =   imgService.setMaskedData(imgReqDto, insData, "qc");
        assertTrue(maskResponse.containsKey("oldUrl") && maskResponse.containsKey("url"));
    }
}